import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pingouin as pg
import seaborn as sns


matrix = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/intensity')
m = matrix[matrix.energy<500]
# sns.catplot(x='vel',y='energy',hue='gait',data=m,col='muscle',row='leg',kind='strip')
# sns.catplot(x='vel',y='energy',hue='gait',data=m,col='muscle',row='leg',kind='box')
# plt.show()


for gait in m.gait.unique():
	for leg in m.leg.unique():
		for muscle in m.muscle.unique():
			subset = m[m.muscle.isin([muscle]) & m.leg.isin([leg]) & m.gait.isin([gait])]
			aov = pg.anova(dv='energy', between='vel', data=subset,detailed=True)
			if aov['p-unc'][0]<.05:
				print('=======================================')
				print(leg+'_'+gait+'_'+muscle)
				print(aov.round(3))

print('-----------------------------')

for gait in m.gait.unique():
	gaitdata = m[m.gait==gait]
	for vel in gaitdata.vel.unique():
		veldata = gaitdata[gaitdata.vel==vel]
		if (gait!='R' and vel!=6.5):
			for muscle in veldata.muscle.unique():
				subset = veldata[veldata.muscle.isin([muscle])]
				x = subset[subset.leg=='tr']['energy'].values
				y = subset[subset.leg=='ld']['energy'].values
				ttest = pg.ttest(x,y)
				if ttest['p-val'][0]<.05:
					print('=======================================')
					print(str(vel)+'_'+gait+'_'+muscle)
					print(ttest.round(3))