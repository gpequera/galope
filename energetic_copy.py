import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
import glob
import os


# folder = '/home/german/ownCloud/work/phd_data/datosphd_npy/'
r = []
subtrials =  ['./00_W_068.npy']
for file in subtrials:
# for file in subtrials:
	file_id = os.path.basename(file)[:-4]
	data = np.load(file,encoding='latin1',allow_pickle=True).item()
	fs_kin = [s for s in data['info'].columns if "fs_kin" in s]
	m = data.get('info')['mass (kg)'].values.item()
	vel = data.get('info')['vel (km/h)'].values.item()/3.6
	fs = data.get('info')[fs_kin].values.item()
	dt = 1/fs
	win = np.array([1/(2*dt),0,-1/(2*dt)])
	dfs=[]
	for ii in data.get('model_output_r'):

		cm = ii[['cm_x','cm_y','cm_z']].reset_index(drop=True)/1000
		dcm = pd.DataFrame()
		dcm['x'] = signal.convolve(cm['cm_x'],win,'same')
		dcm['y'] = signal.convolve(cm['cm_y'],win,'same')
		dcm['z'] = signal.convolve(cm['cm_z'],win,'same')
		dcm.iloc[0] = dcm.iloc[1]
		dcm.iloc[-1] = dcm.iloc[-2]

		stride_duration = cm.shape[0]*dt
		strideFreq = 1/stride_duration
		strideLength = vel/strideFreq

		E = pd.DataFrame()
		E['Eky'] = (m*(dcm['x']+vel)**2)/2
		E['Ekz'] = (m*(dcm['z']**2))/2
		E['mgh'] = m*9.8*cm['cm_z']
		E['Ez'] =  E['mgh'] + E['Ekz']
		E['Etot'] = E['Ez'] + E['Eky']

		W = pd.DataFrame()
		wtot = E['Etot'].diff()
		wtot.iloc[0] = wtot.iloc[1]
		wz = E['Ez'].diff()
		wz.iloc[0] = wz.iloc[1]
		wy = E['Eky'].diff()
		wy.iloc[0] = wy.iloc[1]

		W['Wtot'] = wtot*(wtot>0)
		W['Wz'] = wz*(wz>0)
		W['Wy'] = wy*(wy>0)

		Wext = W.Wtot.sum()/(strideLength*m)
		Wv = W.Wz.sum()/(strideLength*m)
		Wh = W.Wy.sum()/(strideLength*m)
		R = (1-(Wext/(Wv+Wh)))*100

		energetic = pd.DataFrame()
		energetic['recovery'] = [R]
		energetic['Wext'] = [Wext]
		energetic['Wh'] = [Wh]
		energetic['Wv'] = [Wv]
		energetic['subject'] = [file_id[:2]]
		energetic['vel'] = data.get('info')['vel (km/h)'].values.item()
		energetic['gait'] = [file_id[3]]
		energetic['strideFreq'] = [strideFreq]
		energetic['strideLength'] = [strideLength]
		dfs.append(energetic)
	datas = pd.concat(dfs,ignore_index=True).pivot_table(index=['subject','gait','vel'])
	r.append(datas)
alldata = pd.concat(r)
# alldata.reset_index().to_pickle('./energetic.pickle')
sorted_data = alldata.pivot_table(index=['subject','vel'],columns='gait')
print(sorted_data)


f,ax = plt.subplots(2)
plt.subplots_adjust(hspace=0)
stdr = alldata.pivot_table(index='vel',columns=['gait'],values='recovery',aggfunc=np.std)
alldata.pivot_table(index='vel',columns=['gait'],values='recovery').plot(ax=ax[0],yerr=stdr,marker='o',linestyle=':',xlim=[2,12])
ax[0].set_xticks([])
ax[0].set_ylabel('Recovery (%)')
stdwext = alldata.pivot_table(index='vel',columns=['gait'],values='Wext',aggfunc=np.std)
alldata.pivot_table(index='vel',columns=['gait'],values='Wext').plot(ax=ax[1],yerr=stdwext,marker='o',linestyle=':',xlim=[2,12])
ax[1].set_ylabel('Wext (J/kg)')
plt.show()