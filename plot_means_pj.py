import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os

folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/'
file = folder + 'wavelet_means.npy'

means = np.load(file,encoding='latin1',allow_pickle=True).item()

# vel = ['030', '040', '050', '065', '090', '110']

dfs = []

for cc in list(means.keys()):
	for mm in list(means[cc].keys()):
		df = pd.DataFrame()

		value = means[cc][mm].mean(axis=1)
		std = means[cc][mm].std(axis=1)



		df['energy'] = value
		df['std'] = std
		df['muscle'] = [mm]*len(value)
		list_arr = np.float(cc[2:5])/10*np.ones(len(value))
		df['vel'] = list_arr 
		df['leg'] = [cc[-2:]]*len(value)
		df['gait'] = [cc[0]]*len(value)
		df['wavelet'] = np.arange(1,12)
		dfs.append(df)
masterDF = pd.concat(dfs, ignore_index=True)

table = pd.pivot_table(masterDF,index=["wavelet"],columns=["vel","muscle","leg","gait"],values=["energy","std"])


for v in list(table.columns.levels[1]):
	f, ax = plt.subplots(3,2)
	plt.suptitle(v)
	ax = ax.ravel()
	for i,m in zip(range(6),list(table.columns.levels[2])):
		toplot = table["energy"][v][m]
		yerr = table["std"][v][m]
		toplot.plot.bar(ax=ax[i],yerr=yerr)
		ax[i].set_title(m)		

	plt.show()


# for v in vel:
# 	matching = [s for s in list(data.keys()) if v in s]
# 	print(matching)
# 	muscles  = list(data[matching[0]].keys())
# 	fig, ax = plt.subplots(2,4)
# 	ax = ax.ravel()
# 	plt.suptitle(v+' km/h')
# 	mus = {}
# 	for muscle, i in zip(muscles,range(len(muscles))):
# 		legend = []
# 		ax[i].set_title(muscle)
# 		s = pd.DataFrame()
# 		for m in matching:
# 			s[m] = data[m][muscle].mean(axis=1)
# 			s[m].plot(ax=ax[i],label = m)
# 			legend.append(m)
# 		mus.update({muscle:s})
# 	ax[6].legend(legend)


# 	plt.show()


