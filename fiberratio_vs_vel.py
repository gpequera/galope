import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
import os

data = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/fiber_ratio')
W = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/energetic/energetic')
folder_sp = '/home/german/ownCloud/work/phd/proyect_2016/data/colecta/spatiotemporal/*sp'
saveto = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/'


muscles_names = list(data.muscle.unique()) 
n_muscles = len(muscles_names)
colors = ['-b', '--b','-k', '--k', '-r', '--r']

fig1, ax = plt.subplots(4,2,sharex=True)
ax = ax.ravel()
for ii,muscle in enumerate(muscles_names):
	data.pivot_table(index=['muscle','vel'],values='ratio',columns=['gait','leg']).loc[muscle].plot(ax=ax[ii],legend=False,style=colors,marker='.')
	ax[ii].set_title(muscle)
ax[4].set_ylabel('fiber ratio')
ax[6].legend(bbox_to_anchor=(1.5, .7), loc='upper left')
ax[7].axis('off')
plt.savefig(saveto+'fr_vs_vel.pdf')


data.pivot_table(index=['vel'],values='ratio',columns=['gait','leg']).plot(style=colors,marker='.')
plt.title('All muscles')
plt.savefig(saveto+'fr_vs_vel_allmuscles.pdf')

	
trabajo = W.pivot_table(index= ['gait','vel'])
fiber_ratio = data.pivot_table(index=['muscle','gait','leg','vel'],values='ratio')


for leg in ['ld','tr']:
	fig, ax = plt.subplots(4,2,sharex=True)
	ax = ax.ravel()
	for ii,muscle in enumerate(muscles_names):
		for gait,color in zip(['W','R','S'],['b','k','r']):
			x = trabajo.loc[gait]['Wext']
			# y = fiber_ratio.loc[muscle][gait][leg].dropna()
			y = fiber_ratio.loc[muscle].loc[gait].loc[leg]
			ax[ii].plot(x,y,label=gait,color=color, marker='.')
			for i in range(len(x)):
				ax[ii].text(x.values[i],y.values[i],x.index[i])
		ax[ii].set_title(muscle)
	plt.suptitle(leg+ ' leg')
	ax[4].set_ylabel('fiber ratio')
	ax[6].legend(bbox_to_anchor=(1.5, .7), loc='upper left')
	ax[6].set_xlabel('Wext')
	ax[7].axis('off')
	plt.savefig(saveto+'fr_vs_Wext_'+leg+'.pdf')
	# plt.show()


df = []
for file in glob.glob(folder_sp):
	SP = pd.DataFrame()
	file_id = os.path.basename(file)[:-2]
	# sf = pd.read_pickle(file)['Stride Frequency_tr'].mean()
	sp = pd.read_pickle(file).mean()
	SP['subject'] = [file_id[:2]]
	SP['sf'] = [sp['Stride Frequency_tr']]
	SP['df_tr'] = [sp['Duty Factor_tr']]
	SP['df_ld'] = [sp['Duty Factor_lead']]
	SP['vel'] = [np.float(file_id[-3:])/10]
	SP['gait'] = [file_id[3:4]]
	df.append(SP)
spatio = pd.concat(df,ignore_index=True)
ss = spatio.pivot_table(index=['gait','vel'])

for leg, leg_name in zip(['ld','tr'],['Leading','Trailing']):
	fig, ax = plt.subplots(4,2,sharex=True)
	ax = ax.ravel()
	for ii,muscle in enumerate(muscles_names):
		for gait,color in zip(['W','R','S'],['b','k','r']):
			xx = ss.loc[gait]['sf']
			# yy = fiber_ratio.loc[muscle][gait]['tr'].dropna()
			yy = fiber_ratio.loc[muscle].loc[gait].loc[leg]
			ax[ii].plot(xx,yy,label = gait,color=color, marker='.')
			ax[ii].set_title(muscle)
			for i in range(len(x)):
				ax[ii].text(xx.values[i],yy.values[i],xx.index[i])
	ax[4].set_ylabel('fiber ratio')
	ax[7].axis('off')
	ax[6].legend(bbox_to_anchor=(1.5, .7), loc='upper left')
	ax[6].set_xlabel('Stride Freq')
	plt.suptitle(leg_name + ' leg')
	plt.savefig(saveto+'fr_vs_SF_'+leg+'.pdf')
# plt.show()


for leg, leg_name in zip(['ld','tr'],['Leading','Trailing']):
	fig, ax = plt.subplots(4,2,sharex=True)
	ax = ax.ravel()
	for ii,muscle in enumerate(muscles_names):
		for gait,color in zip(['W','R','S'],['b','k','r']):
			xx = ss.loc[gait]['df_'+leg]
			# yy = fiber_ratio.loc[muscle][gait]['tr'].dropna()
			yy = fiber_ratio.loc[muscle].loc[gait].loc[leg]
			ax[ii].plot(xx,yy,label = gait,color=color, marker='.')
			ax[ii].set_title(muscle)
			for i in range(len(x)):
				ax[ii].text(xx.values[i],yy.values[i],xx.index[i])
	ax[4].set_ylabel('fiber ratio')
	ax[7].axis('off')
	ax[6].legend(bbox_to_anchor=(1.5, .7), loc='upper left')
	ax[6].set_xlabel('Duty Factor')
	plt.suptitle(leg_name + ' leg')
	plt.savefig(saveto+'fr_vs_DF_'+leg+'.pdf')

plt.show()