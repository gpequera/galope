#Stiffness stats
import pandas as pd
import numpy as np
import pingouin as pg
from statsmodels.formula.api import ols
import matplotlib.pyplot as plt 
import seaborn as sns
import scikit_posthocs as sp
from itertools import combinations

stiff = pd.read_pickle('./stiffness')
stiff = stiff[~stiff.gait.isin(['W'])]  
stiff['gaitLeg'] = stiff.gait + stiff.leg
stiff = stiff[~stiff.gaitLeg.isin(['Rld'])]
stiff['velmsec'] = np.round(stiff.vel.values/3.6,2)

normal_list = []
levene_list = []

listaanovas = []
listakrukals = []
listapairwises = []

for v in [1.39,  1.81, 2.5]:
	stats = stiff[stiff.velmsec.isin([v])]
	formula = 'k ~ C(gaitLeg)'
	model = ols(formula, data=stats).fit()
	norm = pg.normality(model.resid)
	norm['vel'] = [v]
	# norm['muscle'] = [muscle]
	# norm['n_wavelet'] = [n_wavelet]
	# norm['leg'] = [leg]
	levene = pg.homoscedasticity(stats, dv='k', group='gaitLeg')
	levene['vel'] = [v]
	norm['levene'] = levene['equal_var'].item()
	normal_list.append(norm)
	levene_list.append(levene)
	if len(stats.gaitLeg.unique())> 2:
		if norm.normal[0]:
			if row['levene'] == False:
				anovaGait = pg.welch_anova(data=stats, dv='k', between='gaitLeg').round(3)
				# anovaGait['vel'] = [v]
			else:
				anovaGait = pg.anova(data=stats, dv='k', between='gaitLeg', detailed=True).round(3).drop([1])
			anovaGait['vel'] = [v]
			listaanovas.append(anovaGait)
			if anovaGait['p-unc'][0]<0.05:
				pairwiseGait = pg.pairwise_ttests(dv='k', between='gaitLeg', data=stats, padjust='bonf',parametric=True).round(3)
				pairwiseGait['vel'] = [v] * pairwiseGait.shape[0]
				listapairwises.append(pairwiseGait)
		else:
			kruskalGait = pg.kruskal(data=stats, dv='k', between='gaitLeg', detailed=True).round(3)
			kruskalGait['vel'] = [v]
			listakrukals.append(kruskalGait)
			if kruskalGait['p-unc'][0]<0.05:
				pairwiseGait = pg.pairwise_ttests(dv='k', between='gaitLeg', data=stats, padjust='bonf',parametric=False).round(3)
				pairwiseGait['vel'] = [v] * pairwiseGait.shape[0]
				listapairwises.append(pairwiseGait)
	else:
		pairwiseGait = pg.pairwise_ttests(dv='k', between='gaitLeg', data=stats, padjust='bonf',parametric=norm.normal[0]).round(3)
		pairwiseGait['vel'] = [v] * pairwiseGait.shape[0]
		listapairwises.append(pairwiseGait)

		 

# anovas = pd.concat(listaanovas,ignore_index=True)
kruskals = pd.concat(listakrukals,ignore_index=True)
pairwises = pd.concat(listapairwises,ignore_index=True)
normal = pd.concat(normal_list,ignore_index=True)
levene_test = pd.concat(levene_list,ignore_index=True)





# 		aov = pg.anova(data=stats, dv='k', between='gaitLeg')
# 		if aov['p-unc'][0]<.05:
# 			# posthocs = pg.pairwise_ttests(dv='k', between='gaitLeg',padjust='bonf', data=stats)
# 			posthocs = sp.posthoc_ttest(stats, group_col='gaitLeg',val_col='k',p_adjust='bonf')
# 			tabla = pd.DataFrame()
# 			comp = []
# 			pval = []
# 			for combo1,combo2 in combinations([0,1,2],2):  # 2 for pairs, 3 for triplets, etc
# 			    comp.append(posthocs.index[combo1] + '_vs_' + posthocs.columns[combo2])
# 			    pval.append(posthocs.iloc[combo1,combo2])
# 			tabla['comp'] = comp
# 			tabla['pval'] = pval
# 			tabla['vel'] = [v]*len(comp)
# 			for index, row in tabla.iterrows():
# 				if row['pval'] < .05:
# 					df = pd.DataFrame()
# 					df['vel'] = [v]
# 					df['comp'] = row['comp']
# 					# df['B'] = row['B']
# 					df['pval'] = row['pval']
# 					lista.append(df)
# 	else:
# 		ld = stats[stats.gaitLeg.isin(['Sld'])]['k'].values
# 		tr = stats[stats.gaitLeg.isin(['Str'])]['k'].values
# 		test = pg.ttest(ld,tr)
# 		if test['p-val'].values < .05:
# 			df = pd.DataFrame()
# 			df['vel'] = [v]
# 			df['comp'] = ['Str_vs_Sld']
# 			df['pval'] = test['p-val'].values
# 			lista.append(df)
# stiffstats = pd.concat(lista,ignore_index=True)
# stiffstats.to_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/stiff_stats')


