#!/usr/bin/env python
# coding: utf-8

# In[10]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import glob
import os
from pingouin import ttest
import pingouin as pg

folder = '/home/german/ownCloud/work/phd/proyect_2016/data/colecta/npy/*S*.npy'
files = glob.glob(folder)

dfs = []
thre = 20
for file in files:
    file_id = os.path.basename(file)[:-4]
    data = np.load(file,encoding='latin1',allow_pickle=True).item()
    for npaso in range(15):
        FT = []
        DFl = []
        heels = pd.DataFrame()
        if data['info']['trailing_leg'].item() == 'l':
            heels['tr']  = data['kinematic_l'][npaso]['LMTz']
            heels['ld']  = data['kinematic_l'][npaso]['RMTz']
        else:
            heels['tr']  = data['kinematic_r'][npaso]['RMTz']
            heels['ld']  = data['kinematic_r'][npaso]['LMTz']
        flightLeg = []
        idxInicio = heels['tr'].index[0]
        idxFin = heels['tr'].index[-1]
        idxMedio = int(idxInicio + np.round(heels.shape[0]/2))
        supraThreshold = pd.DataFrame()
        for leg in heels.columns:
            minimo = heels[leg].idxmin()
            maximo = heels[leg].idxmax()
            threshold = heels[leg].loc[minimo] + thre
            supraThreshold[leg] = (heels[leg]>threshold)*1
        intersection = np.where(supraThreshold.loc[idxMedio:]['ld'] == 1)
        fl = idxFin-(intersection[0][0]+idxMedio)
        DFl.append(fl/supraThreshold.shape[0])
    DFT = pd.DataFrame()
    DFT['subject'] = [file_id[0:2]]
    DFT['gait'] = [file_id[3]]
    DFT['vel'] = [np.float(file_id[5:])/10]
    DFT['dutyFlight'] = [np.mean(DFl)]
    dfs.append(DFT)
DutyFlightTime = pd.concat(dfs,ignore_index=True)

spatiotemporal = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/spatiotemporal/spatiotemporal')
spatiotemporal['tcycle_ld'] = spatiotemporal.contactsTime_ld/spatiotemporal.dutyFactor_ld
spatiotemporal['tcycle_tr'] = spatiotemporal.contactsTime_tr/spatiotemporal.dutyFactor_tr
spatiotemporal['dutyFlight'] = 1 -(spatiotemporal.dutyFactor_ld+spatiotemporal.dutyFactor_tr)

#stats
DFTrunning = spatiotemporal[spatiotemporal.gait.isin(['R'])][['subject','gait','vel','dutyFlight']]
DFT = pd.concat([DutyFlightTime,DFTrunning],ignore_index=True)
DFT = DFT[DFT.dutyFlight>0]
DFT.to_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/spatiotemporal/DutyFlightTime')
# toanova = DFT[DFT.vel.isin([6.5])|DFT.vel.isin([9])]
# aov = pg.anova(dv='dutyFlight', between=['gait','vel'], data=toanova,detailed=True)
# pairwise = pg.pairwise_ttests(dv='dutyFlight', between=['vel','gait'], data=toanova).round(3)
# pvals = pairwise['p-unc'].to_list()
# reject, pvals_corr = pg.multicomp(pvals, method='bonf')


for vel in [6.5, 9]:
    df = DFT[DFT.vel.isin([vel])]
    g = df.gait.unique()
    x = df[df.gait.isin([g[0]])]['dutyFlight'].to_numpy()
    y = df[df.gait.isin([g[1]])]['dutyFlight'].to_numpy()
    print(ttest(x, y, tail='two-sided'))

# print(reject, pvals_corr)
fig = plt.figure()
colors = ['#000000',"#1B9E77"]
line = sns.pointplot(x='vel',y='dutyFlight',ci='sd',hue='gait',data=DFT, palette=colors, errwidth=1, capsize=.05)
sns.despine()
sns.set(style="white", font_scale=1.5)
line.text(.95,.40,'*',  fontsize=18)
line.set_xlabel('Speed (m/s)')
line.set_ylabel('Flight time (as a fraction of stride time)')




fig.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/dutyFlight.pdf')
fig.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/dutyFlight.png')

plt.show()
