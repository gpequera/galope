import numpy as np
import os
import glob
from progress.bar import FillingCirclesBar

folder = '/home/german/Nextcloud/work/phd_data/datosphd_npy/*'

files = glob.glob(folder+'*.npy')
n_files = len(files)
bar = FillingCirclesBar('Processing', max=n_files)
conds = []
lista = []
dic = {}
for file in files:
    data = np.load(file,encoding='latin1',allow_pickle=True).item()
    file_name = os.path.basename(file)[:-4]
    lista.append(data)
    bar.next()
    conds.append(file_name)
dic = dict(zip(conds,lista))
bar.finish()
print('Saving...')
np.save('fulldata.npy',dic)