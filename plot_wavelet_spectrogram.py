import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os


folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/'
file = folder+'wavelet_results.npy'
data = np.load(file,encoding='latin1',allow_pickle=True).item()


m = data['CC_W_050tr']['GasMed']['wave'][0]

time_norm = np.arange(0,100,100/m.shape[1])
n_wavelets = range(11)

fig, ax1 = plt.subplots(figsize=(15,6))
ax1.contour(time_norm,n_wavelets,m,cmap= 'coolwarm')
ax1.tick_params(axis='y', labelcolor='b')

ax2 = ax1.twinx()
ax2.plot(time_norm,m.sum(axis=0),color='k', linewidth=.5)
ax2.tick_params(axis='y', labelcolor='k')
plt.show()


