import pandas as pd
import pingouin as pg


mf = pd.read_pickle('./mf.pickle')
mf = mf[~mf.gait.isin(['W'])]
lista = []

for gait in mf.gait.unique():
	for muscle in mf.muscle.unique():
		m = mf[mf.gait.isin([gait]) & mf.muscle.isin([muscle])]
		df = pg.pairwise_ttests(data=m, between='leg', dv='mf')
		df['gait'] = [gait]
		df['muscle'] = [muscle]
		lista.append(df)
statsLeg = pd.concat(lista,ignore_index=True)
lista = []

for leg in mf.leg.unique():
	for muscle in mf.muscle.unique():
		m = mf[mf.leg.isin([leg]) & mf.muscle.isin([muscle])]
		df = pg.pairwise_ttests(data=m, between='gait', dv='mf')
		df['leg'] = [leg] 
		df['muscle'] = [muscle]
		lista.append(df)
statsGait = pd.concat(lista,ignore_index=True)
