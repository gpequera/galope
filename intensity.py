import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import glob
import os

# folder1 = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/'
# data = {}
# for file in glob.glob(folder1 + '*.npy'):
#     name = os.path.basename(file)[8:-4]
#     dic = np.load(file,allow_pickle=True,encoding='latin1').item()
#     data.update({name:dic})

data = np.load('wavelet_res.npy',encoding='latin1',allow_pickle=True).item()
keystonorm = [s for s in data.keys() if 'R_065' in s]
# norm = {k:v for k,v in data.items() if k in keystonorm}


dfs = []
for trial in list(data.keys()):
	for mm in list(data[trial].keys()):
		df = pd.DataFrame()
		lista = []
		for i, ii in enumerate(data[trial][mm]['wave']):
			# ii = n/n.mean()
			subjectfornorm1 = [s for s in keystonorm if trial[:2] in s]
			subjectfornorm = [s for s in subjectfornorm1 if trial[-2:] in s][0]
			datafornorm = data[subjectfornorm][mm]['wave']
			lista.append((ii.sum().sum()/datafornorm[i].sum().sum())*100)
		value = np.mean(lista)
		df['energy'] = [value]
		df['muscle'] = [mm]
		df['gait'] = [trial[3]]
		df['leg'] = [trial[-2:]]
		df['subject'] = [trial[:2]]
		df['trial_id'] = trial
		df["vel"] = np.float(trial[5:8])/10
		dfs.append(df)
matrix = pd.concat(dfs,ignore_index=True)
matrix.to_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/intensity')
m = matrix[matrix.energy<500]
sns.catplot(x='vel',y='energy',hue='gait',data=m,col='muscle',row='leg',kind='strip')
sns.catplot(x='vel',y='energy',hue='gait',data=m,col='muscle',row='leg',kind='box')
plt.show()
