import pandas as pd
import numpy as np
wavelet_data = pd.read_pickle('./wavelet_table') 


# df=pd.DataFrame()
column_names = []
df = wavelet_data[wavelet_data.muscle.isin(['GasMed']) & wavelet_data['#_wavelet'].isin([1]) & wavelet_data['leg'].isin(['ld'])][['subject','gait','vel','energy']].set_index(['subject','gait','vel'])

for muscle in wavelet_data.muscle.unique():
	for n_wavelet in wavelet_data['#_wavelet'].unique():
		for leg in wavelet_data.leg.unique():
			df = df.join(wavelet_data[wavelet_data.muscle.isin([muscle]) & wavelet_data['#_wavelet'].isin([n_wavelet]) & wavelet_data['leg'].isin([leg])][['subject','gait','vel','energy']].set_index(['subject','gait','vel']),lsuffix=muscle+str(n_wavelet)+leg)
df.drop(['energy'], axis=1,inplace=True)


spt = pd.read_pickle('/home/german/Documents/repo/papersyn/spatiotemporal').set_index(['subject','gait','vel'])
energetic = pd.read_pickle('/home/german/Documents/repo/papersyn/energetic')
wint = pd.read_pickle('/home/german/Documents/repo/papersyn/wint')
wint['velkm']=np.round(wint.vel*3.6,1)
wint = wint.drop(['vel'],axis=1)
wint.rename(columns={'velkm':'vel'}, inplace=True)
wint= wint.set_index(['subject','gait','vel'])
mf = pd.read_pickle('./medianfreq.pickle').set_index(['subject','gait','vel'])
asymm = pd.read_pickle('./assymetries.pickle').set_index(['subject','gait','vel'])


table = spt.join(energetic).join(df).join(wint).join(mf).join(asymm[['Tsteptr','Tstepld','assymt','Lsteptr','Lstepld','assyml']])
# table = energetic.join(df).join(wint).join(mf).join(asymm)
table.reset_index(inplace=True)
table.to_pickle('./table.pickle')