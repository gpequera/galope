import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import numpy as np
import pingouin as pg


table = pd.read_pickle('./table.pickle')
table = table[~table.gait.isin(['W'])]
meaned = table.groupby(['gait','vel']).mean().reset_index()
std = table.groupby(['gait','vel']).std().reset_index()
muscles = ['GasMed', 'SOL','RF', 'VasMed', 'TIB', 'BF', 'GluMed']
colors = ['r','b']
marker = ['o','*']


for variable in ['strideFreq','Wext','Wint']:
	fig, ax = plt.subplots(2,4,sharex=True, sharey=True)
	fig.suptitle(variable)
	ax=ax.ravel()
	for iii,gait in enumerate(meaned.gait.unique()):
		m = meaned[meaned.gait.isin([gait])]
		mm = table[table.gait.isin([gait])]
		s = std[std.gait.isin([gait])]
		x = m[variable]
		xx = mm[variable]
		xerr = s[variable]
		for i,muscle in enumerate(muscles):
			for ii,side in enumerate(['ld','tr']):
				y = m['mf_'+muscle+ '_' +side]
				yy = mm['mf_'+muscle+ '_' +side]
				yerr = s['mf_'+muscle+ '_' +side]
				ylabel = 'mf_'+muscle+ '_' +side
				ax[i].set_title(muscle)
				ax[i].errorbar(x, y, xerr=xerr, yerr=yerr, fmt=colors[ii], marker=marker[iii],label=gait+side,
								ls='none',ecolor=(.5,.5,.5),markersize=10, elinewidth=.5,markeredgecolor='k')
				ax[i].plot(xx,yy,color=colors[ii], marker=marker[iii],markersize=4,ls='none',alpha=0.3)
				ax[i].set_xlabel(variable)
				ax[i].set_ylabel(ylabel[:-3])
			if i == 6:
				ax[i].legend()
		ax[7].axis('off')
	


for variable in ['dutyFactor_', 'contactsTime_']:
	fig, ax = plt.subplots(2,4,sharex=True, sharey=True)
	ax=ax.ravel()
	for ii,side in enumerate(['ld','tr']):
		for iii,gait in enumerate(meaned.gait.unique()):
			m = meaned[meaned.gait.isin([gait])]
			mm = table[table.gait.isin([gait])]
			s = std[std.gait.isin([gait])]
			xlabel = variable+side
			x = m[xlabel]
			xx = mm[xlabel]
			xerr = s[xlabel]
			for i,muscle in enumerate(muscles):
				ylabel = 'mf_'+muscle+ '_' +side
				y = m[ylabel]	
				yy = mm[ylabel]	
				yerr = s[ylabel]
				ax[i].errorbar(x, y, xerr=xerr, yerr=yerr, fmt=colors[ii], marker=marker[iii],
								label=gait+side,ls='none',ecolor=(.5,.5,.5),
								markersize=10, elinewidth=.5,markeredgecolor='k')
				ax[i].plot(xx,yy,color=colors[ii], marker=marker[iii],markersize=4,ls='none',alpha=0.3)
				ax[i].set_xlabel(xlabel[:-3])
				ax[i].set_ylabel(ylabel[:-3])
			if i == 6:
				ax[i].legend()
	ax[7].axis('off')
# plt.show()

lista = [s  for s in list(table.columns) if 'mf' in s]

for variable in lista:
	for gait in table.gait.unique():
		aov = pg.anova(dv=variable, between='vel', data=table[table.gait==gait],detailed=True)
		if aov['p-unc'][0]<0.05:
			print('=======================================')
			print(variable+'_'+gait)
			print(aov.round(3))
