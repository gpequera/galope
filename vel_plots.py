import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


file = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wavelet_table'
data = pd.read_pickle(file)
stats = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/vel_stats')
muscles = data.muscle.unique()
# colors = [['#5899DA', '#E8743B'],['#BF399E', '#5899DA', '#E8743B'],['#BF399E','#5899DA' ]]



for c,gait in enumerate(['Walking','Skipping','Running']):
	bygait = data[data.gait.isin([gait[0]])]
	sns.set()
	g = sns.FacetGrid(bygait, col="muscle",row='leg',margin_titles=True, height=3,aspect=1)
	sns.set_style("white")
	g = g.map(sns.barplot, '#_wavelet',"energy",'vel',ci='sd',errwidth=.7,capsize=.05,
			  hue_order=bygait.vel.sort_values().unique(),order=bygait['#_wavelet'].unique(),palette=['k','grey',[.8,.8,.8],'white'],edgecolor='k')
	g.add_legend()
	g.despine()
	# sideSkip_stats = leg_stats[leg_stats.gait.isin(['S']) & leg_stats.vel.isin([v])]
	# for muscle in sideSkip_stats.muscle.unique():
	# 	figNumber = g.col_names.index(muscle)
	# 	for wave in sideSkip_stats[sideSkip_stats.muscle.isin([muscle])]['#_wavelet'].unique():
	# 		g.axes.flatten()[figNumber].text(wave-1,.3,'*')
	plt.suptitle(gait)
	plt.show()