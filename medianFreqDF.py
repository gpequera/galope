import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os

folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/' 

lista = []

files = glob.glob(folder+'*.npy')
for file in files:
	data = np.load(file,allow_pickle=True,encoding='latin1').item()
	filename = os.path.basename(file)
	for muscle in data.keys():
		# print(filename)
		# print(muscle)
		df = pd.DataFrame()
		df['mf'] =  [np.mean(data[muscle]['MF'])]
		df['muscle'] = [muscle]
		df['gait'] = [filename[11]]
		df['subject'] = [filename[8:10]]
		df['leg'] = [filename[-6:-4]]
		df['vel'] = np.float(filename[13:16])/10 


		lista.append(df)
mf = pd.concat(lista,ignore_index=True)
mf.to_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/mf')
mf.to_pickle('./mf.pickle')

