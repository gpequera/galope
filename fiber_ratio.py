import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os
import statsmodels.api as sm
from statsmodels.formula.api import ols
from statsmodels.stats.multicomp import MultiComparison
from scipy import stats



folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/'
dflista = []
for file in glob.glob(folder+'*.npy'):
	files = os.path.basename(file)
	dic = np.load(file,encoding='latin1',allow_pickle=True).item()
	muscles = list(dic.keys())
	n_strides = len(dic['GasMed']['pj']) 
	for muscle in muscles:
		lista = []
		for pj in dic[muscle]['pj']:
			lista.append(pj[5:].sum()/pj[1:].sum())
		df = pd.DataFrame()
		df['ratio'] = [np.array(lista).mean()]
		df['muscle'] = [muscle]
		df['leg'] = [files[-6:-4]]
		df['gait'] = [files[11:12]]
		df['gait_leg'] = [files[11:12]+'_'+files[-6:-4]]
		df['vel'] = [np.float(files[13:16])/10]
		dflista.append(df)
DF = pd.concat(dflista,ignore_index=True)
DF.to_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/fiber_ratio')

table = DF.pivot_table(index=['vel','muscle'],columns=['gait_leg'],values=['ratio'], aggfunc='mean')
std = DF.pivot_table(index=['vel','muscle'],columns=['gait_leg'],values=['ratio'], aggfunc='std')

# toplot.plot.bar(yerr=yerr,color=color, edgecolor='k',legend=False, error_kw=dict(ecolor='black',elinewidth=0.5, lolims=True))

fig, ax = plt.subplots(3,sharex=True)
table.loc[5].plot.bar(ax=ax[0],yerr=std.loc[5],legend=False)
table.loc[6.5].plot.bar(ax=ax[1],yerr=std.loc[6.5])
table.loc[9].plot.bar(ax=ax[2],yerr=std.loc[9],legend=False)

plt.show()

# stats
for vel in [5,6.5,9]:
	for m in muscles:
		tostats = DF[DF.vel.isin([vel]) & DF.muscle.isin([m])]  
		results = ols('ratio ~ C(gait_leg)', data=tostats).fit()
		aov_table = sm.stats.anova_lm(results, typ=2)
		if aov_table['PR(>F)'][0]<.05:
			print(str(aov_table['PR(>F)'][0])+' --> '+str(vel)+'km/h - '+m)
			mod = MultiComparison(tostats['ratio'],tostats['gait_leg'])
			tab = mod.allpairtest(stats.ttest_ind)[0]
			matrix = pd.read_html(tab.as_html(),header=0,index_col=0)[0]
			# print(mod.allpairtest(stats.ttest_ind)[0])
			print(matrix[matrix['reject'] == True])

fig1 = plt.figure()
DF.pivot_table(index=['muscle','vel'],values='ratio',columns=['gait','leg']).loc['RF'].plot()
plt.show()