import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import pingouin as pg

m = pd.read_pickle('./assymetries.pickle')
m['velmsec'] = np.round(m.vel/3.6,2)
#clear outlayers
# lista = list(m[m.assyml>1].index)
# data = m.drop(lista)

colors = ["#D95F02","#1B9E77",'#000000']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]

sns.set(style="white", font_scale=1)
err_kws = {'elinewidth':1,'capsize':3}
ylabel = ['Temporal asymmetry \n index','Spatial asymmetry \n index']
fig1,ax1 = plt.subplots(1,2,figsize=(6,2.5),sharey=True)
# plt.subplots_adjust(wspace=.4)

hue_order=['R','S']
for i,variable in enumerate(['assymt','assyml']):
	sns.boxplot(x='velmsec',y=variable,data=data,hue='gait', hue_order=hue_order, ax=ax1[i],palette= "Set2")
	sns.swarmplot(x="velmsec", y=variable, hue="gait", hue_order=hue_order, data=data,palette="Set2",
		ax=ax1[i])
	ax1[i].set_ylabel(ylabel[i])
	ax1[i].set_xlabel('Speed (m.sec$^{-1}$)')
	ax1[i].legend_.remove()
	for gait in data.gait.unique():
		aov = pg.anova(dv=variable, between='vel', data=data[data.gait==gait],detailed=True)
		print('=======================================')
		print(variable+'_'+gait)
		print(aov.round(3))
handles, labels = ax1[1].get_legend_handles_labels()
l = plt.legend(handles[0:2], labels[0:2], bbox_to_anchor=(0.8, 0.3), loc=2, borderaxespad=0.,frameon=False)
sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.despine()
sns.set_style("ticks")
plt.savefig('./figures/asymmetries.pdf')
plt.savefig('./figures/asymmetries.png')
plt.show()
