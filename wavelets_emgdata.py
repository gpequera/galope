#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:41:52 2019

@author: german
"""
import numpy as np
import matplotlib.pyplot as plt
import borg_wavelet as emgwave
import pandas as pd
from scipy.signal import butter, filtfilt
import glob
import os
from progress.bar import FillingCirclesBar

b, a = butter(4, [.02, .5], btype='band')
path = '/home/german/Nextcloud/work/datosphd_npy/two_sides_emg/'
folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/'
if not os.path.exists(folder):
        os.makedirs(folder)
folder1 = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/'
# folder1 = '~/Nextcloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/'
if not os.path.exists(folder1):
        os.makedirs(folder1)
subjects = dict()
files = glob.glob(path + '*.npy')
n_files = len(files)
bar = FillingCirclesBar('Processing', max=n_files)
for file in files:
	filename = os.path.basename(file)[:-4]
	dic = np.load(file,encoding='latin1',allow_pickle=True).item()
	key = list(dic.keys())[0]
	data = dic[key]
	dicc = {}
	for muscle in list(data[0].columns):
		pj = []
		MF = []
		wave = []
		PJ = dict()
		n_pasos = len(data)
		pasos = int(np.round(n_pasos)/2)-10
		for ii in range(pasos,pasos+20):
			s = data[ii][muscle]
			s_offset = s-s.mean()
			s_f = filtfilt(b,a,s_offset)
			II, pj_N, mf = emgwave.borg_wavelet_der(s_f)
			wave.append(II)
			pj.append(pj_N)
			MF.append(mf)
		PJ = {'pj':pj,'MF':MF,'wave':wave}
		dicc.update({muscle:PJ})
	np.save(folder1+'wavelet_'+ filename + '.npy',dicc)
	bar.next()
bar.finish()