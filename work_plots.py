import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import pingouin as pg

energetic = pd.read_pickle('../papersyn/energetic').reset_index()
energetic['velmsec'] = np.round(energetic.vel/3.6,2)
W = pd.read_pickle('../papersyn/W')
W = W.drop(list(W[W.wext>2].index))

subset = W[W.gait=='W']
energetic = energetic[energetic.gait=='W']
sns.set(style="white", font_scale=1)
err_kws = {'elinewidth':1,'capsize':3}
ylabel = ['Wext (J/Kg.m)','Wint (J/Kg.m)','Wtotal (J/Kg.m)']
fig1,ax1 = plt.subplots(2,2,figsize=(6,6),sharex=True)
ax1 = ax1.ravel()
hue_order = ['R', 'S']
for i,variable in enumerate(['wext','wint','wtotal']):
	sns.stripplot(x='vel',y=variable,data=subset, ax=ax1[i],palette= "Set2", s=2.5)
	sns.pointplot(x="vel", y=variable, data=subset,markers='*',linestyles='--',palette="Set2",
		ax=ax1[i],jitter=True,dodge=True,ci='sd',errwidth=1,scale=0.6)
	ax1[i].set_ylabel(ylabel[i])
	#ax1[i].legend_.remove()
	for gait in subset.gait.unique():
		aov = pg.anova(dv=variable, between='vel', data=subset[subset.gait==gait])
		print('=======================================')
		print(variable+'_'+gait)
		print(aov.round(3))

sns.stripplot(x='velmsec',y='recovery',data=energetic, ax=ax1[3],palette= "Set2", s=2.5)
sns.pointplot(x="velmsec", y='recovery', data=energetic,markers='*',linestyles='--',palette="Set2",
	ax=ax1[3],jitter=True,dodge=True,ci='sd',errwidth=1,scale=0.6)
ax1[3].set_ylabel('Recovery (%)')
ax1[0].set_xlabel(None)
ax1[1].set_xlabel(None)
ax1[2].set_xlabel('Speed (m.s$^{-1}$)')
ax1[3].set_xlabel('Speed (m.s$^{-1}$)')


handles, labels = ax1[3].get_legend_handles_labels()
l = plt.legend(handles[0:2], labels[0:2], bbox_to_anchor=(0.8, 0.3), loc=2, borderaxespad=0.,frameon=False)
sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.despine()
sns.set_style("ticks")
plt.savefig('./figures/work.pdf')
plt.savefig('./figures/work.png')
plt.show()
