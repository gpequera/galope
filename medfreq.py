import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os

folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/' 


sidedict = {}
for side in ['tr','ld']:
	lista = []
	files = glob.glob(folder+'*'+side+'.npy')
	for file in files:
		data = np.load(file,allow_pickle=True,encoding='latin1').item()
		filename = os.path.basename(file)
		df = pd.DataFrame()
		df['gait'] = [filename[11]]
		df['subject'] = [filename[8:10]]
		df['vel'] = np.float(filename[13:16])/10
		for muscle in data.keys():
			df['mf_'+ muscle + '_' + filename[-6:-4]] =  [np.mean(data[muscle]['MF'])]
		lista.append(df)
	
	sidedict.update({side:pd.concat(lista,ignore_index=True).set_index(['subject','gait','vel'])})
mf = sidedict['tr'].join(sidedict['ld']).reset_index()
mf.to_pickle('./medianfreq.pickle')
