import pandas as pd
import pingouin as pg
import statsmodels.api as sm
from statsmodels.formula.api import ols
import numpy as np

wint = pd.read_pickle('../wint.pickle')
wint.sort_values(by=['subject','gait','vel'], inplace=True)
wint.reset_index(drop=True,inplace=True)
W = pd.read_pickle('../W.pickle')
work = W.join(wint[['sup_ld', 'sup_tr', 'inf_tr', 'inf_ld']])
work['Wtotal'] = W.Wint+W.Wext
work['WintRel'] = W.Wint/(W.Wint+W.Wext)
work['WextRel'] = W.Wext/(W.Wint+W.Wext)


# W5 = W[W.vel==1.39]
# W65 = W[W.vel==1.81]
# W9 = W[W.vel==2.5]
# WR = W[W.gait=='R']
# WS = W[W.gait=='S']
# WW = W[W.gait=='W']



lista = []
listalevene = []
for vel in [1.39,1.81,2.5]:
	for var1 in ['Wext', 'Wint', 'Wtotal','inf_tr','inf_ld','recovery']:
		formula = var1+'~ C(gait)'
		model = ols(formula, data=work[work.velmsec==vel]).fit()
		norm = pg.normality(model.resid)
		norm['vel'] = [vel]
		norm['var'] = [var1]
		levene = pg.homoscedasticity(work[work.velmsec==vel], dv=var1, group='gait')
		levene['vel'] = [vel]
		levene['var'] = [var1]
		norm['levene'] = levene['equal_var'].item()
		lista.append(norm)
		listalevene.append(levene)

	
normal = pd.concat(lista,ignore_index=True)
normal = normal[['var', 'vel','W', 'pval', 'normal', 'levene']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
levene_test = pd.concat(listalevene,ignore_index=True)
levene_test = levene_test[['var', 'vel', 'W', 'pval', 'equal_var']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
# for vel in [1.81,2.5]:


listaanovas = []
listakrukals = []
listapairwises = []
for index, row in normal.iterrows():
	dv = row['var']
	vel = row['vel']
	for data in [work]:
		subset= data[data.velmsec==vel]
		if dv in list(data.columns):
			if  len(subset.gait.unique())>2:
				if row['normal'] == True:
					if row['levene'] == False:
						anovaGait = pg.welch_anova(data=subset, dv=dv, between='gait').round(3)
						anovaGait['condition'] = [str(dv)+'-'+'- '+str(vel)]
						anovaGait['var'] = [str(dv)]
						anovaGait['vel'] = [str(vel)]
						listaanovas.append(anovaGait)
					else:
						anovaGait = pg.anova(data=subset, dv=dv, between='gait', detailed=True).round(3).drop([1])
						anovaGait['condition'] = [str(dv)+'-'+'- '+str(vel)]
						anovaGait['var'] = [str(dv)]
						anovaGait['vel'] = [str(vel)]
						listaanovas.append(anovaGait)
					if anovaGait['p-unc'][0]<0.05:
						pairwiseGait = pg.pairwise_ttests(dv=dv, between='gait', data=subset, padjust='bonf',parametric=True).round(3)
						pairwiseGait['condition'] = [str(dv)+'-'+'- '+str(vel)] * pairwiseGait.shape[0]
						pairwiseGait['var'] = [str(dv)]  * pairwiseGait.shape[0]
						pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
						listapairwises.append(pairwiseGait)
				else:
					kruskalGait = pg.kruskal(data=subset, dv=dv, between='gait', detailed=True).round(3)
					kruskalGait['condition'] = [str(dv)+'-'+'- '+str(vel)]
					kruskalGait['var'] = [str(dv)]
					kruskalGait['vel'] = [str(vel)]
					listakrukals.append(kruskalGait)
					if kruskalGait['p-unc'][0]<0.05:
						pairwiseGait = pg.pairwise_ttests(dv=dv, between='gait', data=subset, padjust='bonf',parametric=False).round(3)
						pairwiseGait['condition'] = [str(dv)+'-'+'- '+str(vel)] * pairwiseGait.shape[0]
						pairwiseGait['var'] = [str(dv)]  * pairwiseGait.shape[0]
						pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
						listapairwises.append(pairwiseGait)
			else:
				pairwiseGait = pg.pairwise_ttests(dv=dv, between='gait', data=subset, padjust='bonf',parametric=row['normal']).round(3)
				pairwiseGait['condition'] = [str(dv)+'-'+'- '+str(vel)] * pairwiseGait.shape[0]
				pairwiseGait['var'] = [str(dv)]  * pairwiseGait.shape[0]
				pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
				listapairwises.append(pairwiseGait)


if listaanovas:
	anovas = pd.concat(listaanovas,ignore_index=True).round(3)
	anovas = anovas[['var', 'vel','Source', 'SS', 'DF', 'MS', 'F', 'p-unc', 'np2', 'ddof1','ddof2']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
if listakrukals:	
	kruskals = pd.concat(listakrukals,ignore_index=True).round(3)
	kruskals = kruskals[['var', 'vel','Source', 'ddof1', 'H', 'p-unc']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)

if listapairwises:
	pairwises = pd.concat(listapairwises,ignore_index=True).round(3)
	pairwises = pairwises[['var','vel','Contrast','A','B','Parametric','p-unc','p-corr','p-adjust']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)


# for letra in ['A','B']:
# 	pairwises[letra] = pairwises[letra].replace('S','Str')		


