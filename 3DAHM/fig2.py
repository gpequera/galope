import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pingouin as pg
from statsmodels.formula.api import ols
import statsmodels.api as sm

wint = pd.read_pickle('../wint.pickle')
wint.sort_values(by=['subject','gait','vel'], inplace=True)
wint.reset_index(drop=True,inplace=True)
W = pd.read_pickle('../W.pickle')
work = W.join(wint[['sup_ld', 'sup_tr', 'inf_tr', 'inf_ld']])
work['Wtotal'] = W.Wint+W.Wext
work['WintRel'] = W.Wint/(W.Wint+W.Wext)
work['WextRel'] = W.Wext/(W.Wint+W.Wext)


intworks = work[['subject','gait','velmsec','sup_ld','sup_tr','inf_tr','inf_ld']]
data = intworks.melt( id_vars=["subject",'gait','velmsec'], value_vars=["sup_ld", "sup_tr",'inf_tr','inf_ld'])
dataS = data[data.gait=='S']
dataSinf = dataS[dataS.variable.isin(['inf_ld','inf_tr'])]
# WintLimbs = dataSinf[dataSinf.velmsec.isin([vel])]

fig1, ax1= plt.subplots(1,3,figsize=(8,2.5))
sns.despine();
for i, vel in enumerate([1.39, 1.81, 2.5]):
	sns.scatterplot(x='variable',y='value',data=dataSinf[dataSinf.velmsec.isin([vel])],
				ax=ax1[i])
	sns.lineplot(x='variable',y='value',data=dataSinf[dataSinf.velmsec.isin([vel])],
				ax=ax1[i],units='subject', estimator=None,color=(.85,.85,.85))
	ax1[i].set_ylabel('Work (J/kg.m)')
	ax1[i].set_xlabel(str(vel) + ' m/s')
	ax1[i].set_xticklabels(['Trailing','Leading'])



##stats

formula = 'value~ C(variable)'
model = ols(formula, data=dataSinf).fit()
norm = pg.normality(model.resid)
levene = pg.homoscedasticity(data=dataSinf, dv='value', group='variable')

anova = pg.anova(data=dataSinf, dv='value', between='variable', detailed=True).round(3)
for i,vel in enumerate([1.39, 1.81, 2.5]):
	pairwise = pg.pairwise_ttests(dv='value', between='variable', data=dataSinf[dataSinf.velmsec==vel], padjust='bonf',parametric=True).round(3)
	p = pairwise['p-unc'][0]
	print(pairwise)
	ax1[i].set_title('p = ' + str(p))



fig1.tight_layout()
fig1.savefig('./fig2.pdf')
plt.show()
