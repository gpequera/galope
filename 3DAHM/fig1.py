import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np


wint = pd.read_pickle('../wint.pickle')
wint.sort_values(by=['subject','gait','vel'], inplace=True)
wint.reset_index(drop=True,inplace=True)
W = pd.read_pickle('../W.pickle')
work = W.join(wint[['sup_ld', 'sup_tr', 'inf_tr', 'inf_ld']])
work['Wtotal'] = W.Wint+W.Wext
work['WintRel'] = W.Wint/(W.Wint+W.Wext)
work['WextRel'] = W.Wext/(W.Wint+W.Wext)

plt.style.use('seaborn-paper')
fig, ax = plt.subplots(2,3, sharex=True, figsize=(8,6))
ax = ax.ravel()

vels = np.sort(work.velmsec.unique())
columnas = ['Wtotal','Wext','Wint', 'inf_tr', 'inf_ld']
titles = ['Wtotal','Wext','Wint', 'WtrailingLeg', 'WleadingLeg']
for i, col in enumerate(columnas):
	sns.lineplot(x='velmsec', y=col, hue='gait',data=work, ax = ax[i], ci='sd',marker='o')
	ax[i].set_title(titles[i])
	ax[i].get_legend().remove()
	ax[i].set_xticks(vels) 
	ax[i].set_xticklabels(vels,rotation=45)
	ax[i].set_ylabel('J/kg.m')

	if i>2:
		ax[i].sharey(ax[3])
	if i>2:
		ax[i].set_xlabel('Speed (m/s)')
		ax[i].set_ylim([0,.3])
	else:
		ax[i].xaxis.label.set_visible(False)

# ax[2].sharey(ax[1])
ax[0].set_ylim([0,2.25])
ax[1].set_ylim([0,2.25])
ax[2].set_ylim([0,2.25])
ax[3].set_ylim([0,.4])
ax[4].set_ylim([0,.4])
# ax[5].set_ylim([0,.4])

for i,letter in enumerate(['A','B','C']):
	ax[i].text(.83,2.1,letter)
for i,letter in zip([3,4],['D','E']):
	ax[i].text(.83,.38,letter)
ax[5].text(.83,.13,'F')

q = pd.read_pickle('./q.pickle')

sns.lineplot(x='velmsec',y='value',hue='gait',style='variable', ci='sd',data=q,ax=ax[5],marker='o')

ax[5].set_xticks(vels) 
ax[5].set_xticklabels(vels,rotation=45)
ax[5].set_xlabel('Speed (Km/h)')
ax[5].set_title('q value')
ax[5].yaxis.label.set_visible(False)
ax[5].legend(bbox_to_anchor=(1.04,1), borderaxespad=0)


#asteriscos

ax[0].text(1.39,2, '*', fontsize=8)
ax[0].text(1.81,2, '* †', fontsize=8)
ax[0].text(2.5,2, '#', fontsize=8)


ax[1].text(1.39,2, '*', fontsize=8)
ax[1].text(1.81,2, '* # †', fontsize=8)
ax[1].text(2.5,2, '#', fontsize=8)

ax[2].text(1.39,2, '*', fontsize=8)
ax[2].text(1.81,2, '#', fontsize=8)
ax[2].text(2.5,2, '#', fontsize=8)

ax[3].text(1.39,.35, '*', fontsize=8)
ax[3].text(1.81,.35, '#', fontsize=8)
ax[3].text(2.5,.35, '#', fontsize=8)

ax[4].text(1.39,.35, '*', fontsize=8)
ax[4].text(1.81,.35, '# †', fontsize=8)
ax[4].text(2.5,.35, '#', fontsize=8)

ax[5].text(1.39,.12, '¥', fontsize=8)
ax[5].text(1.81,.12, '¥', fontsize=8)
ax[5].text(2.5,.12, '¥', fontsize=8)




fig.tight_layout()
fig.savefig('./fig1.pdf')
# plt.show()
work.to_csv('./work.pickle')
# print(work.groupby(['gait','vel']).mean()[['WintRel','WextRel']])

