import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pingouin as pg
from statsmodels.formula.api import ols
import statsmodels.api as sm

wint = pd.read_pickle('../wint.pickle')
wint.sort_values(by=['subject','gait','vel'], inplace=True)
wint.reset_index(drop=True,inplace=True)
W = pd.read_pickle('../W.pickle')
work = W.join(wint[['sup_ld', 'sup_tr', 'inf_tr', 'inf_ld']])
work['Wtotal'] = W.Wint+W.Wext
work['WintRel'] = W.Wint/(W.Wint+W.Wext)
work['WextRel'] = W.Wext/(W.Wint+W.Wext)
work.sort_values(by=['subject','gait','vel']).reset_index(inplace=True, drop=True)

duty = pd.read_pickle('./spatiotemporal.pickle')

tr = duty[duty.leg=='tr'].sort_values(by=['subject','gait','vel']).reset_index()[['subject', 'gait', 'vel', 'dutyFactor']].rename(columns={'dutyFactor': 'df_tr'})
ld = duty[duty.leg=='ld'].sort_values(by=['subject','gait','vel']).reset_index()[['subject', 'gait', 'vel', 'dutyFactor']].rename(columns={'dutyFactor': 'df_ld'})

data = work.join(ld['df_ld']).join(tr['df_tr'])



wint = data['Wint']
sf = data['strideFreq']
vel = data['velmsec']
DFtr = data['df_tr']/100
DFld = data['df_ld']/100


data['qtr'] = wint/(sf*vel*(1+(DFtr/(1-DFtr)**2)))
data['qld'] = wint/(sf*vel*(1+(DFld/(1-DFld)**2)))

data.to_pickle('./alldata.pickle')
q = data[['subject','gait','velmsec','qtr','qld']].melt(id_vars=['subject','gait','velmsec'],value_vars=['qtr','qld'])
q.to_pickle('./q.pickle')



qS = q[q.gait=='S']


lista = []
listalevene = []
formula = 'value~ C(variable)'

for vel in [1.39,1.81,2.5]:
	# for var1 in ['Wext', 'Wint', 'Wtotal','inf_tr','inf_ld','recovery']:
	model = ols(formula, data=qS[qS.velmsec==vel]).fit()
	norm = pg.normality(model.resid)
	norm['vel'] = [vel]
	# norm['var'] = [var1]
	levene = pg.homoscedasticity(qS[qS.velmsec==vel], dv='value', group='variable')
	levene['vel'] = [vel]
	# levene['var'] = [var1]
	norm['levene'] = levene['equal_var'].item()
	lista.append(norm)
	listalevene.append(levene)

	
normal = pd.concat(lista,ignore_index=True)
normal = normal[['vel','W', 'pval', 'normal', 'levene']].sort_values(by=['vel']).round(3).reset_index(drop=True)
levene_test = pd.concat(listalevene,ignore_index=True)
levene_test = levene_test[['vel', 'W', 'pval', 'equal_var']].sort_values(by=['vel']).round(3).reset_index(drop=True)

anova = pg.anova(data=qS, dv='value', between='variable', detailed=True).round(3)
print(anova)
for i,vel in enumerate([1.39, 1.81, 2.5]):
	pairwise = pg.pairwise_ttests(dv='value', between='variable', data=qS[qS.velmsec==vel], padjust='bonf',parametric=True).round(3)
	print(pairwise)