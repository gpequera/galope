import numpy as np
import pandas as pd
import glob
import os

directory = '/home/german/ownCloud/work/phd/proyect_2016/data/colecta/npy/'
files = glob.glob(directory+'*.npy')

for file in files:
	file_name = os.path.basename(file)
	data = np.load(file,allow_pickle=True,encoding='latin1').item()
	for ciclor in data['emg_r']:
		if 'Sensor 1' in list(ciclor.columns):
			ciclor.columns = ['GasMedR', 'SOLR', 'RFR', 'VasMedR', 'TIBR', 'BFR', 'GluMedioR', 'GasMedL', 'SOLL', 'RFL', 'VasMedL', 'TIBL', 'BFL', 'GluMedL']
	for ciclol in data['emg_l']:
		if 'Sensor 1' in list(ciclol.columns):
			ciclol.columns = ['GasMedR', 'SOLR', 'RFR', 'VasMedR', 'TIBR', 'BFR', 'GluMedioR', 'GasMedL', 'SOLL', 'RFL', 'VasMedL', 'TIBL', 'BFL', 'GluMedL']
	np.save(directory+file_name,data)