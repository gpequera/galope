import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pingouin as pg


k = pd.read_pickle('./stiffness')
k['gaitLeg'] = k['gait']+k['leg']
subset = k[k.gaitLeg.isin(['Str','Sld','Rld' ])]

skipping = subset[subset.gait=='S']
trailing = skipping[skipping.leg=='tr'].sort_values(by=['subject','vel'])
leading = skipping[skipping.leg=='ld'].sort_values(by=['subject','vel'])
df = leading[['subject','vel','gait']]
df['asymmkleg'] = (trailing.kleg.values-leading.kleg.values)/leading.kleg.values
df['asymmFmax'] = trailing.Fmax.values-leading.Fmax.values
df['asymml'] = trailing.deltaL.values-leading.deltaL.values



fig, ax = plt.subplots(2,3,sharex=True,figsize=(10,7))
ax = ax.ravel()
hue_order = ['Rld','Sld', 'Str']
for i,variable in enumerate(['kleg','Fmax','deltaL']):
	sns.stripplot(x="vel", y=variable, hue="gaitLeg", hue_order=hue_order, data=subset, jitter=True,dodge=True,palette="Set2", edgecolor=None, linewidth=1,ax=ax[i],alpha=1,size=2.5)
	# sns.boxplot(x="vel", y=variable, hue="gaitLeg", data=subset,palette="Set2",fliersize=0,ax=ax[i])
	sns.pointplot(x="vel", y=variable, hue="gaitLeg", hue_order=hue_order, data=subset,markers='*',linestyles='--',palette="Set2",
		ax=ax[i],jitter=True,dodge=True,ci='sd',errwidth=1,scale=0.6)
	ax[i].legend().remove()
	for gait in subset.gaitLeg.unique():
		aov = pg.anova(dv=variable, between='vel', data=subset[subset.gaitLeg==gait],detailed=True)
		print('=======================================')
		print(variable+'_'+gait)
		print(aov.round(3))

for ii,variable in enumerate(['asymmkleg','asymmFmax','asymml']):
	sns.stripplot(x="vel", y=variable, data=df, color='k', edgecolor=None, linewidth=1,ax=ax[ii+3],alpha=1,size=2.5)
	# sns.boxplot(x="vel", y=variable, hue="gaitLeg", data=subset,palette="Set2",fliersize=0,ax=ax[i])
	sns.pointplot(x="vel", y=variable,  data=df,markers='*',linestyles='--',color='k',
		ax=ax[ii+3],ci='sd',errwidth=1,scale=0.6)
	aov = pg.anova(dv=variable, between='vel', data=df,detailed=True)
	print('=======================================')
	print(variable)
	print(aov.round(3))
handles, labels = ax[2].get_legend_handles_labels()
l = ax[2].legend(handles[0:3], labels[0:3], bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.,frameon=False)

#-------------------------------
#Stats





plt.savefig('./figures/stiffness.pdf')
plt.savefig('./figures/stiffness.png')
plt.show()
