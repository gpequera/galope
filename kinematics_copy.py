#kinematics

''' Paquete de funciones para hacer cálculos cinemáticos '''


import numpy as np


def angulos(m,a,b,c):
    
    Ax = m[a+'x']-m[b+'x']
    Ay = m[a+'y']-m[b+'y']
    Az = m[a+'z']-m[b+'z']
    Al = np.sqrt(Ax**2+Ay**2+Az**2)

    Bx = m[a+'x']-m[c+'x']
    By = m[a+'y']-m[c+'y']
    Bz = m[a+'z']-m[c+'z']
    Bl = np.sqrt(Bx**2+By**2+Bz**2)

    Cx = m[b+'x']-m[c+'x']
    Cy = m[b+'y']-m[c+'y']
    Cz = m[b+'z']-m[c+'z']
    Cl = np.sqrt(Cx**2+Cy**2+Cz**2)

    angle = np.arccos((Al**2+Bl**2-Cl**2)/(2*Al*Bl))


    return angle

def angulos_abs(m,distal,proximal):
    #ATAN2(y proximal – y distal, z proximal – z distal)
    angle = np.arctan(m[distal+'z'] - m[proximal+'z'], m[distal+'x'] - m[proximal+'x'])
    return angle
