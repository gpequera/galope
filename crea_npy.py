"Crea diccionario npy para poder correr los scripts de wint y wext"
"G. Pequera 4/2022"



import pandas as pd
import numpy as np


cm = pd.read_csv('~/Downloads/CM.csv')
markers = pd.read_csv('~/Downloads/Markers.csv')
info = pd.DataFrame()
info['mass (kg)'] = [float(76.1)]
info["fs_kin"] = [float(100)]
info['vel (km/h)'] = [float(6.8)]
info['gait'] = ['W']
info['trailing_leg'] = ['r']
data = {}
data = {'info':info, 'model_output_r':[cm], 'kinematic_r':[markers]}

np.save('./00_W_068.npy',data)