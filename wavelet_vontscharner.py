# -------------------------------------------------------------------------
# INPUT:    y     ... 1xn (or nx1) vector, signal of length n
#           J     ... integer, number of wavelets
#           q     ... double, parameter
#           r     ... double, parameter
#           scale ... double, parameter
#           fs    ... double, sampling rate
# 
# OUTPUT:   W     ... Jxn matrix, wavelet transform of signal y
#           mf    ... double, mean frequency
#           Fpsi  ... Jxn matrix, wavelet-operator on Fourier side
#           cf    ... Center frequency
# USAGE:    This routine performs a wavelet transform of your signal using
#           the family of wavelets as suggested in
# 
#           "Intensity analysis in time-frequency space of surface
#           myoelectric signals by wavelets of specified resolution",
#           Journal of Electromyography and Kinesiology by Vinzenz von
#           Tscharner [vT].
# 
#           A similar routine, using another wavelet family (namely Morlet
#           wavelets) can be found in
# 
#           "Filter banks and the 'Intensity Analysis' of EMG" by F.Borg on
#           arxiv.org [Bo].
# 
#         
# 
#           OUTPUT: -------------------------------------------------------
#           This routine needs a signal as an input, furthe input is
#           optional. By default the values suggested in [vT] will be used
#           to create the wavelet family. The output is the matrix W,
#           containing your wavelet transformed signal in the time-scale
#           domain. Summing up the rows of the matrix gives an
#           approximation of the original signal by your wavelet family.
# 
#           Second output argument is the (by absolute mass of amplitude)
#           weighted mean-frequency (mf) of your signal.
# 
#           The last output is the matrix Fpsi, which is the Fourier
#           transform of your wavelet-operator. Row J contains your wavelet
#           with scale J-1. The wavelets on the Fourier-side are
#           constructed in such a way, that their sum is close to constant
#           in (at least) an area of 20-200 Hz (see [vT]).
# 
# -------------------------------------------------------------------------
# LITERATURE:
# [vT]: http://dx.doi.org/10.1016/S1050-6411(00)00030-4,
# [Bo]: http://arxiv.org/ftp/arxiv/papers/1005/1005.0696.pdf
# 
# SEE ALSO: EMG_WAVEMF.M, EMG_INTPLOT.M
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.ndimage import gaussian_filter1d



def borg_wavelet_der(y, J=11, q=1.45, r=1.959, scale=0.3, fs=2000):

    ###
    ### defining default values as suggested

    
        # cf = [6.9 19.29 37.71 62.09 92.36 128.48 170.39 218.08 271.5 330.63 395.46]; #center-frequencies of the wavelets
        # fs = 2000; #sampling rate
        # J = 11; #number of wavelets
        # q = 1.45; #parameter chosen simultaneously to q to have sum of wavelets const. by least square appr.
        # r = 1.959; #parameter chosen simultaneously to r to have sum of wavelets const. by least square appr.
        # scale = 0.3; #parameter chosen for EMG, not to be confused with the scale concept for wavelets
    

    ############################################
    ###
    ### center frequencies for non-default input

    I = np.arange(J)
    cf = np.zeros([len(I)])
    for kk in I:
        cf[kk] = (I[kk]+q)**r/scale
    

    ######################################################################
    ###
    ### going from time to frequency (Fourier) side;

    N = 2*len(y);
    Fy = np.fft.fft(y,N)
    f = np.arange(N)*fs/N;
    Fpsi = np.zeros([J,N])

    #########################################################
    ###
#########################################################
###
### defining the wavelets on the frequency (Fourier) side

    for kk in range(J):
        Fpsi[kk,:] = ((f/cf[kk])**(scale*cf[kk]))*np.exp(( (-f)/(cf[kk]) + 1 )*cf[kk]*scale )
    
    ########################################################################
    ###
    ### multiplying on frequency (Fourier) side <-> convolution on time side
    
    Fw = np.zeros([J,N])
    WW = np.zeros([J,N])
    ii = np.zeros([J,N])
    II = np.zeros([J,N//2])
    res_temporal=np.array([0.0765,0.059,0.0405,0.0315, 0.026, 0.0215, 0.0195, 0.0165, 0.0150, 0.0135, 0.0120])
    #Los datos de resolución temporal fueron extraídos del paper de Von Tscharner


    for hh in range(J):
        Fw[hh,:] = Fy.real*Fpsi[hh,:]           
        WW[hh,:] = np.fft.ifft(Fw[hh,:]).real                       # transformada de wavelet        
        derivada = ((1/(2*np.pi*cf[hh]))*np.fft.ifft(2*np.pi *f*Fw[hh,:]).real)**2 
        ii[hh,:] = (WW[hh,:]*WW[hh,:].conj()) +  derivada          # Acá hay que ajustar un poco
        II[hh,:] = gaussian_filter1d(ii[hh,:N//2],fs*res_temporal[hh]*(3/8))
        
    II = pd.DataFrame(II.T)
    II.drop(columns=0,inplace=True)
    #############################
    ##
    # calculating mean frequency
    
    pj = np.sum(II,axis=0);#taking total absolute mass of amplitudes as weight
    E_total = np.sum(pj)
    pj_N = pj/E_total
    
    
    mf = np.dot(cf[1:],pj)/np.sum(pj) #weighted mean of all frequencies

    return II, pj_N, mf