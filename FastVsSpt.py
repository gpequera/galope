import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

spt = pd.read_pickle('./../papersyn/spt')
data = pd.read_pickle('./wavelet_table')

muscles = data.muscle.unique()

spt_grouped = spt.groupby(['gait','vel','leg']).mean().reset_index().sort_values(by=['gait','vel','leg'])
m = data.groupby(['gait','vel','leg','muscle','#_wavelet']).mean().reset_index()
data_grouped = m[m['#_wavelet']>3].groupby(['gait','vel','leg','muscle']).sum().reset_index()


f1,ax1 = plt.subplots(2,4)
ax1 = ax1.ravel()
f2,ax2 = plt.subplots(2,4)
ax2 = ax2.ravel()
for i, muscle in enumerate(muscles):
	toplot = data_grouped[data_grouped.muscle.isin([muscle])].reset_index().join(spt_grouped[['dutyFactor','stride_freq','stride_len']])
	g1 = sns.scatterplot(x='stride_freq', y='energy',data=toplot[(toplot.gait=='S') | (toplot.gait=='R')],hue='leg',style='gait',ax=ax1[i],size='vel',legend=False)
	g2 = sns.scatterplot(x='dutyFactor', y='energy',data=toplot[toplot.gait=='S'],hue='leg',style='gait',ax=ax2[i],size='vel',legend=False)
	ax1[i].set_title(muscle)
	ax2[i].set_title(muscle)
ax1[6].legend(loc='center left', bbox_to_anchor=(1.25, 0.5), ncol=3)
plt.show()
