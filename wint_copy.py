# Biomecanica trabajo final


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import kinematics as kin
import glob
import seaborn as sns
from progress.bar import FillingCirclesBar
import os


trials = '/home/german/Nextcloud/work/phd_data/datosphd_npy/*.npy'
carpeta = glob.glob(trials)
# borrar = [s for s in carpeta if "GP_R_065" in s][0]
# carpeta.remove(borrar)
bar = FillingCirclesBar('Processing', max=len(carpeta))
subtrials =  ['./00_W_068.npy']
#cargo archivo de datos

listdf = []
Wintbyframe = {} 


segmentos = ['brazo_tr', 'brazo_ld', 
             'antbrazo_tr','antbrazo_ld', 
             'muslo_tr','muslo_ld',
             'pierna_tr','pierna_ld', 
             'pie_tr','pie_ld']

segmentosXYZ = ['pierna_der_x',  'pierna_der_y', 'pierna_der_z', 
                'pierna_izq_x', 'pierna_izq_y', 'pierna_izq_z',
                'muslo_der_x', 'muslo_der_y', 'muslo_der_z', 
                'muslo_izq_x', 'muslo_izq_y', 'muslo_izq_z',
                'pie_der_x', 'pie_der_y', 'pie_der_z',
                'pie_izq_x', 'pie_izq_y', 'pie_izq_z',
                'brazo_der_x', 'brazo_der_y', 'brazo_der_z', 
                'brazo_izq_x', 'brazo_izq_y', 'brazo_izq_z',
                'antbrazo_der_x', 'antbrazo_der_y', 'antbrazo_der_z', 
                'antbrazo_izq_x', 'antbrazo_izq_y', 'antbrazo_izq_z']
SegmentosDerechos = [s for s in segmentosXYZ if 'der' in s]
SegmentosIzquierdos = [s for s in segmentosXYZ if 'izq' in s]

for trial in subtrials:
    data = np.load(trial,allow_pickle=True,encoding='latin1').item()
    info = data['info']
    file_name = os.path.basename(trial)[:-4]
    fs = [s for s in data['info'].columns if "fs_kin" in s]
    masa=data['info']['mass (kg)'][0]
    m=pd.DataFrame(data = {'brazo_tr':[masa*0.028],
                               'brazo_ld':[masa*0.028],
                               'antbrazo_tr':[masa*0.016],
                               'antbrazo_ld':[masa*0.016],
                               'muslo_tr':[masa*0.100],
                               'muslo_ld':[masa*0.100],
                               'pierna_tr':[masa*0.0465],
                               'pierna_ld':[masa*0.0465],
                               'pie_tr':[masa*0.0145],
                               'pie_ld':[masa*0.0145]})
    # Defino dt y ventana de convolucion
    fs_kin = info[fs].values[0][0]
    dt = float(1/fs_kin)
    win = np.array([1/(2*dt),0,-1/(2*dt)])
    #vel = strideFreq*strideLen
    vel = np.round(float(info['vel (km/h)'])/3.6,2)
    
    listaWint = []
    listLimbdf = []
    bar.next()

    if info.trailing_leg.values[0].lower() == 'r':
        trail = 'r'
        trasera = 'der'
        lead = 'l'
        delantera = 'izq'
        SegTra = SegmentosDerechos
        SegDel = SegmentosIzquierdos
    else:
        trail = 'l'
        trasera = 'der'
        lead = 'r'
        delantera = 'izq'
        SegDel = SegmentosDerechos
        SegTra = SegmentosIzquierdos

    WintLimbByframe = []
    
    for paso, marcadores in zip(data['model_output_'+trail][:20],data['kinematic_'+trail][:20]):
    
        N = paso.shape[0]
        strideDur = N*dt
        strideFreq = 1/strideDur
        strideLen = vel/strideFreq
        marcadores =marcadores/1000
        k=pd.DataFrame(data = {'brazo_tr':[0.322]*np.sqrt((marcadores[trail.upper()+'ELBx']-marcadores[trail.upper()+'SHx'])**2+(marcadores[trail.upper()+'ELBy']-marcadores[trail.upper()+'SHy'])**2+(marcadores[trail.upper()+'ELBz']-marcadores[trail.upper()+'SHz'])**2),
                               'brazo_ld':[0.322]*np.sqrt((marcadores[lead.upper()+'ELBx']-marcadores[lead.upper()+'SHx'])**2+(marcadores[lead.upper()+'ELBy']-marcadores[lead.upper()+'SHy'])**2+(marcadores[lead.upper()+'ELBz']-marcadores[lead.upper()+'SHz'])**2),
                               'antbrazo_tr':[0.303]*np.sqrt((marcadores[trail.upper()+'WRx']-marcadores[trail.upper()+'ELBx'])**2+(marcadores[trail.upper()+'WRy']-marcadores[trail.upper()+'ELBy'])**2+(marcadores[trail.upper()+'WRz']-marcadores[trail.upper()+'ELBz'])**2),
                               'antbrazo_ld':[0.303]*np.sqrt((marcadores[lead.upper()+'WRx']-marcadores[lead.upper()+'ELBx'])**2+(marcadores[lead.upper()+'WRy']-marcadores[lead.upper()+'ELBy'])**2+(marcadores[lead.upper()+'WRz']-marcadores[lead.upper()+'ELBz'])**2),
                               'muslo_tr':[0.323]*np.sqrt((marcadores[trail.upper()+'KNx']-marcadores[trail.upper()+'GTx'])**2+(marcadores[trail.upper()+'KNy']-marcadores[trail.upper()+'GTy'])**2+(marcadores[trail.upper()+'KNz']-marcadores[trail.upper()+'GTz'])**2),
                               'muslo_ld':[0.323]*np.sqrt((marcadores[lead.upper()+'KNx']-marcadores[lead.upper()+'GTx'])**2+(marcadores[lead.upper()+'KNy']-marcadores[lead.upper()+'GTy'])**2+(marcadores[lead.upper()+'KNz']-marcadores[lead.upper()+'GTz'])**2),
                               'pierna_tr':[0.302]*np.sqrt((marcadores[trail.upper()+'ANx']-marcadores[trail.upper()+'KNx'])**2+(marcadores[trail.upper()+'ANy']-marcadores[trail.upper()+'KNy'])**2+(marcadores[trail.upper()+'ANz']-marcadores[trail.upper()+'KNz'])**2),
                               'pierna_ld':[0.302]*np.sqrt((marcadores[lead.upper()+'ANx']-marcadores[lead.upper()+'KNx'])**2+(marcadores[lead.upper()+'ANy']-marcadores[lead.upper()+'KNy'])**2+(marcadores[lead.upper()+'ANz']-marcadores[lead.upper()+'KNz'])**2),
                               'pie_tr':[0.475]*np.sqrt((marcadores[trail.upper()+'MTx']-marcadores[trail.upper()+'HEEx'])**2+(marcadores[trail.upper()+'MTy']-marcadores[trail.upper()+'HEEy'])**2+(marcadores[trail.upper()+'MTz']-marcadores[trail.upper()+'HEEz'])**2),
                               'pie_ld':[0.475]*np.sqrt((marcadores[lead.upper()+'MTx']-marcadores[lead.upper()+'HEEx'])**2+(marcadores[lead.upper()+'MTy']-marcadores[lead.upper()+'HEEy'])**2+(marcadores[lead.upper()+'MTz']-marcadores[lead.upper()+'HEEz'])**2)})
        k.reset_index(inplace=True, drop=True)

        relativeCm = pd.DataFrame()
        relativeVelocity = pd.DataFrame()

        for seg,leg in zip([SegTra,SegDel],['_tr','_ld']):
            for se in seg:
                relativeCm[se[:-6]+leg+se[-2:]] = (paso['cm_'+se] - paso['cm_'+se[-1]])/1000
                relativeVelocity[se[:-6]+leg+se[-2:]] = sig.convolve(relativeCm[se[:-6]+leg+se[-2:]],win,'same')
        relativeCm.reset_index(inplace=True, drop=True)
        
        relativeVelocity.iloc[0] = relativeVelocity.iloc[1]
        relativeVelocity.iloc[N-1] = relativeVelocity.iloc[N-2]
        relativeVelocity.reset_index(inplace=True, drop=True)

        cinetica  = pd.DataFrame()

        for segXYZ in relativeVelocity.columns:
            cinetica[segXYZ] = (1/2)*((relativeVelocity[segXYZ]**2)*float(m[segXYZ[:-2]]))
        cinetica.reset_index(inplace=True, drop=True)


        angles = pd.DataFrame()
        angles_abs = pd.DataFrame()

        for lado,side in zip(['_ld', '_tr'],[lead.upper(),trail.upper()]):
            angles['brazo'+lado] = kin.angulos(marcadores,side+'SH',side+'ELB',side+'GT')
            angles['antbrazo'+lado] = kin.angulos(marcadores,side+'ELB',side+'SH',side+'WR')
            angles['muslo'+lado] = kin.angulos(marcadores,side+'GT',side+'SH',side+'KN') 
            angles['pierna'+lado] = kin.angulos(marcadores,side+'KN',side+'GT',side+'AN')
            angles['pie'+lado] = kin.angulos(marcadores,side+'AN',side+'KN',side+'MT')

            angles_abs['brazo'+lado] = kin.angulos_abs_leo(marcadores,side+'ELB',side+'SH')
            angles_abs['antbrazo'+lado] = kin.angulos_abs_leo(marcadores,side+'WR',side+'ELB')
            angles_abs['muslo'+lado] = kin.angulos_abs_leo(marcadores,side+'KN',side+'GT') 
            angles_abs['pierna'+lado] = kin.angulos_abs_leo(marcadores,side+'AN',side+'KN')
            angles_abs['pie'+lado] = kin.angulos_abs_leo(marcadores,side+'MT',side+'AN')

        angles.reset_index(inplace=True, drop=True)
        angles_abs.reset_index(inplace=True, drop=True)
        
        for col in angles_abs.columns:
            for n,i in enumerate(angles_abs[col]):
                if i<0:
                    angles_abs[col][n] = i + 2*np.pi
        
        dangles = pd.DataFrame()
        dangles_abs = pd.DataFrame()

        for col in angles.columns:
            dangles[col] = sig.convolve(angles[col],win,'same')
            dangles_abs[col] = sig.convolve(angles_abs[col],win,'same')
        dangles.loc[0] = dangles.loc[1]
        dangles.loc[N-1] = dangles.loc[N-2]

        dangles_abs.loc[0] = dangles_abs.loc[1]
        dangles_abs.loc[N-1] = dangles_abs.loc[N-2]
        
        


        I = pd.DataFrame()

        for segm in m.columns:    
            I[segm] = (float(m[segm]))*(k[segm]**2)
            cinetica[segm+'Rot'] = (1/2)*I[segm]*(dangles_abs[segm]**2)
 
        cineticatotal = pd.DataFrame()
        
        cineticatotal['muslo_tr'] = cinetica[['muslo_tr_z','muslo_tr_y','muslo_trRot']].sum(axis=1)#,'brazo_derRot_x']].sum(axis=1)
        cineticatotal['muslo_ld'] = cinetica[['muslo_ld_z','muslo_ld_y','muslo_ldRot']].sum(axis=1)#,'brazo_izqRot_x']].sum(axis=1)
        cineticatotal['pierna_tr'] = cinetica[['pierna_tr_z','pierna_tr_y','pierna_trRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
        cineticatotal['pierna_ld'] = cinetica[['pierna_ld_z','pierna_ld_y','pierna_ldRot']].sum(axis=1)#,'antbrazo_izqRot_x']].sum(axis=1)
        cineticatotal['pie_tr'] = cinetica[['pie_tr_z','pie_tr_y','pie_trRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
        cineticatotal['pie_ld'] = cinetica[['pie_ld_z','pie_ld_y','pie_ldRot']].sum(axis=1)
        cineticatotal['brazo_tr'] = cinetica[['brazo_tr_z','brazo_tr_y','brazo_trRot']].sum(axis=1)#,'brazo_derRot_x']].sum(axis=1)
        cineticatotal['brazo_ld'] = cinetica[['brazo_ld_z','brazo_ld_y','brazo_ldRot']].sum(axis=1)#,'brazo_izqRot_x']].sum(axis=1)
        cineticatotal['antbrazo_tr'] = cinetica[['antbrazo_tr_z','antbrazo_tr_y','antbrazo_trRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
        cineticatotal['antbrazo_ld'] = cinetica[['antbrazo_ld_z','antbrazo_ld_y','antbrazo_ldRot']].sum(axis=1)#,'antbrazo_izqRot_x']].sum(axis=1)


        cineticaBylimb = pd.DataFrame()
        cineticaBylimb['sup_ld'] = cineticatotal[['brazo_ld','antbrazo_ld']].sum(axis=1)
        cineticaBylimb['sup_tr'] = cineticatotal[['brazo_tr','antbrazo_tr']].sum(axis=1)
        cineticaBylimb['inf_tr'] = cineticatotal[['muslo_tr','pierna_tr','pie_tr']].sum(axis=1)
        cineticaBylimb['inf_ld'] = cineticatotal[['muslo_ld','pierna_ld','pie_ld']].sum(axis=1)

 
        incrementos = cineticaBylimb.diff()
        IncrementosPositivos = ((incrementos>0)*incrementos)/(masa*strideLen)
        WintLimb = IncrementosPositivos.sum()
        WintLimbByframe.append(sig.resample(IncrementosPositivos.values[1:,:],100))
        listaWint.append(WintLimb.sum())
        pormiembro = WintLimb.to_frame().T
        pormiembro['Wint'] = WintLimb.sum()
        listLimbdf.append(pormiembro)

    allstrides = pd.concat(listLimbdf,ignore_index=True)
    WINT = pd.DataFrame()
    WINT = allstrides.mean().to_frame().T
    byframe = pd.DataFrame(np.mean(WintLimbByframe,axis=0),columns=IncrementosPositivos.columns)
    Wintbyframe.update({file_name:byframe})

    WINT['subject'] = [file_name[:2]]
    WINT['vel'] = [vel]
    WINT['gait'] = [info.gait[0].upper()]
    listdf.append(WINT)
bar.finish()
Wint = pd.concat(listdf,ignore_index=True)
# Wint.to_pickle('./wint.pickle')
# np.save('/home/german/ownCloud/work/phd/proyect_2016/data/energetic/wintByframe.npy',Wintbyframe)
# sns.lineplot(x='vel',y='Wint',data=Wint,hue='gait',ci='sd')
# plt.show()