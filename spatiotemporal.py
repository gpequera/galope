import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
import glob
import os

folder = '/home/german/Nextcloud/work/phd_data/datosphd_npy/'
DFS = []
for file in glob.glob(folder+'*.npy'):
	file_id = os.path.basename(file)[:-4]
	data = np.load(file,encoding='latin1',allow_pickle=True).item()
	info = data.get('info')
	m = info['mass (kg)'].values.item()
	trLeg = info['trailing_leg'].values.item()
	vel = info['vel (km/h)'].values.item()
	fsample = [s for s in data['info'].columns if "fs_kin" in s]
	fs = info[fsample].values.item()
	dt = 1/fs
	# subject = info['file_name'].values.item()[:2].upper()
	gait = info['gait'].values.item().upper()
	dfs={}
	contacts={}
	freqs= {}
	lengths = {}
	for side in ['l','r']:	
		lista1 = []
		lista2 = []
		lista3 = []
		lista4 = []
		for ii in data.get('kinematic_' + side)[:20]:
			s = ii[side.upper()+'MTz']
			idxInicio = s.index[0]
			idxFin = s.index[-1]
			minimo = s.idxmin()
			maximo = s.idxmax()
			threshold = s.loc[minimo] + 20
			sCutted = s.iloc[:-20]
			idxToeoff = sCutted[sCutted<threshold].index[-1]
			cycle_dur = (idxFin - idxInicio)/fs
			stride_freq = 1/cycle_dur
			stride_len = (vel/3.6)/stride_freq
			duty = (idxToeoff - idxInicio)/cycle_dur	
			contact = (idxToeoff - idxInicio)/fs
			lista1.append(duty)
			lista2.append(contact)
			lista3.append(stride_freq)
			lista4.append(stride_len)
		# dfs.update({side:np.mean(lista1)}) 
		# contacts.update({side:np.mean(lista2)})
		# freqs.update({side:np.mean(lista3)})
		# lengths.update({side:np.mean(lista4)})
		DF = pd.DataFrame()
		DF['subject'] = [file_id[:2]]
		DF['gait'] = [gait]
		DF['vel'] = [vel]
		DF['dutyFactor'] = [np.mean(lista1)]
		DF['contactsTime'] = [np.mean(lista2)]
		DF['stride_freq'] = [np.mean(lista3)]
		DF['stride_len'] = [np.mean(lista4)]
		if trLeg == side:
			DF['leg'] = ['tr']
		else:
			DF['leg'] = ['ld']


		DFS.append(DF)
dutyFactor = pd.concat(DFS,ignore_index=True,sort=False)
dutyFactor.to_pickle('./3DAHM/spatiotemporal.pickle')
