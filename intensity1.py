import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import glob
import os

folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/'


b = []
for file in glob.glob(folder+'*.npy'):
	name = os.path.basename(file)[8:-4]
	data = np.load(file,allow_pickle=True,encoding='latin1').item()
	for m in data:
		a = []
		c = []
		for ii in data[m]['wave']:
			mean = ii.sum(axis=1).mean()
			suma = ii.sum().sum()
			a.append(suma)
			c.append(mean)
		# print([np.mean(a),np.mean(c),m])
		df = pd.DataFrame()
		df['intensity'] = [np.mean(a)]
		df['meanintensity'] = [np.mean(c)]
		df['subject'] = [name[:2]]
		df['gait'] = [name[3]]
		df['vel'] = np.float(name[5:8])/10
		df['leg'] = [name[-2:]]
		df['muscle'] = [m]
		b.append(df)
intensity = pd.concat(b,ignore_index=True)
intensity.to_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/intensity')