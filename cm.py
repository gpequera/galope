#!/usr/bin/env python
# coding: utf-8

# In[68]:


import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import os
import glob
import tkinter as tk
from tkinter import filedialog
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual


# In[69]:



folder = '../../kinematics_pandas/cm_pandas/'
if not os.path.exists(folder):
    os.makedirs(folder)


# In[70]:


for file in glob.glob('../../kinematics_pandas/raw/*'):
    s = pd.read_pickle(file)   
    m = np.array(s)
    xr = m[:,range(0,27,3)]
    yr = m[:,range(1,27,3)]
    zr = m[:,range(2,27,3)]
    xl = m[:,range(27,54,3)]
    yl = m[:,range(28,54,3)]
    zl = m[:,range(29,54,3)]

    #CENTER OF MASS CALCULATION (calculation made on the right side of the body)

    #      TRUNK & HEAD
    #      proximal: GT; distal: LOB.

    xth_r = xr[:,4] + (.66*(xr[:,0]-xr[:,4]))
    xth_l = xl[:,4] + (.66*(xl[:,0]-xl[:,4]))
    yth_r = yr[:,4] + (.66*(yr[:,0]-yr[:,4]))
    yth_l = yl[:,4] + (.66*(yl[:,0]-yl[:,4]))
    zth_r = zr[:,4] + (.66*(zr[:,0]-zr[:,4]))
    zth_l = zl[:,4] + (.66*(zl[:,0]-zl[:,4]))

    #     % UPPER ARM
    #     % proximal: SH; distal: EL.

    xua_r = xr[:,1] + (0.436*(xr[:,2]-xr[:,1]))
    yua_r = yr[:,1] + (0.436*(yr[:,2]-yr[:,1]))
    zua_r = zr[:,1] + (0.436*(zr[:,2]-zr[:,1]))
    xua_l = xl[:,1] + (0.436*(xl[:,2]-xl[:,1]))
    yua_l = yl[:,1] + (0.436*(yl[:,2]-yl[:,1]))
    zua_l = zl[:,1] + (0.436*(zl[:,2]-zl[:,1]))

    #     % FOREARM & HAND
    #     % proximal: EL; distal: WR.

    xfah_r = xr[:,2] + (0.682*(xr[:,3]-xr[:,2]))
    yfah_r = yr[:,2] + (0.682*(yr[:,3]-yr[:,2]))
    zfah_r = zr[:,2] + (0.682*(zr[:,3]-zr[:,2]))
    xfah_l = xl[:,2] + (0.682*(xl[:,3]-xl[:,2]))
    yfah_l = yl[:,2] + (0.682*(yl[:,3]-yl[:,2]))
    zfah_l = zl[:,2] + (0.682*(zl[:,3]-zl[:,2]))

    #     % THIGHT
    #     % proximal: GT; distal: KN.

    xt_r = xr[:,4] + (0.433*(xr[:,5]-xr[:,4]))
    yt_r = yr[:,4] + (0.433*(yr[:,5]-yr[:,4]))
    zt_r = zr[:,4] + (0.433*(zr[:,5]-zr[:,4]))
    xt_l = xl[:,4] + (0.433*(xl[:,5]-xl[:,4]))
    yt_l = yl[:,4] + (0.433*(yl[:,5]-yl[:,4]))
    zt_l = zl[:,4] + (0.433*(zl[:,5]-zl[:,4]))

    #     % LEG
    #     % proximal: KN; distal: AN.

    xlg_r = xr[:,5] + (0.433*(xr[:,6]-xr[:,5]))
    ylg_r = yr[:,5] + (0.433*(yr[:,6]-yr[:,5]))
    zlg_r = zr[:,5] + (0.433*(zr[:,6]-zr[:,5]))
    xlg_l = xl[:,5] + (0.433*(xl[:,6]-xl[:,5]))
    ylg_l = yl[:,5] + (0.433*(yl[:,6]-yl[:,5]))
    zlg_l = zl[:,5] + (0.433*(zl[:,6]-zl[:,5]))

    #     % FOOT
    #     % proximal: AN; distal: MT.

    xfoo_r = xr[:,6] + (0.5*(xr[:,8]-xr[:,6]))
    yfoo_r = yr[:,6] + (0.5*(yr[:,8]-yr[:,6]))
    zfoo_r = zr[:,6] + (0.5*(zr[:,8]-zr[:,6]))
    xfoo_l = xl[:,6] + (0.5*(xl[:,8]-xl[:,6]))
    yfoo_l = yl[:,6] + (0.5*(yl[:,8]-yl[:,6]))
    zfoo_l = zl[:,6] + (0.5*(zl[:,8]-zl[:,6]))  

    right_ankle = xr[:,6]

    #     %Total center of mass
    xcm  = np.array((xth_r*(0.578/2))+(xth_l*(0.578/2))+ (xua_r*0.028)+ (xfah_r*0.022)+ (xt_r*0.1) + (xlg_r*0.0465) + (xfoo_r*0.0145)+(xua_l*0.028)+ (xfah_l*0.022)+ (xt_l*0.1) + (xlg_l*0.0465) + (xfoo_l*0.0145))

    ycm  = np.array((yth_r*(0.578/2))+ (yth_r*(0.578/2))+(yua_r*0.028)+ (yfah_r*0.022)+ (yt_r*0.1) + (ylg_r*0.0465) + (yfoo_r*0.0145)+ (yua_l*0.028)+ (yfah_l*0.022)+ (yt_l*0.1) + (ylg_l*0.0465) + (yfoo_l*0.0145))

    zcm  = np.array((zth_r*(0.578/2))+ (zth_r*(0.578/2))+(zua_r*0.028)+ (zfah_r*0.022)+ (zt_r*0.1) + (zlg_r*0.0465) + (zfoo_r*0.0145)+ (zua_l*0.028)+ (zfah_l*0.022)+ (zt_l*0.1) + (zlg_l*0.0465) + (zfoo_l*0.0145))


    cm = np.array([xcm,ycm,zcm]).T
    # np.savetxt(file[:-4]+'_cm.csv',zcm,delimiter=',')
    #     cm = np.array([xcm,ycm,zcm]).T
    cm = pd.DataFrame(cm)
    cm.columns = ['x','y','z']
    cm.to_pickle(folder+os.path.basename(file)+'_cm')
    # np.savetxt(file[:-4]+'_cm.csv',zcm,delimiter=',')


# In[71]:


cm


# In[ ]:




