import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

# intensity = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/intensity')
# cot = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/k5_data/CoT')
# energy = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wavelet_table')
stiffnes = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/stiffnes')
stiffstats = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/stiff_stats')
data = pd.read_pickle('./wavelet_table')
data['velmsec'] = np.round(data.vel/3.6,2)
data = data[~data.gait.isin(['W'])]
gait_stats = pd.read_pickle('./stats/gait_stats_SvsR.pickle')
vel_stats = pd.read_pickle('./stats/vel_stats.pickle')
leg_stats = pd.read_pickle('./stats/leg_stats.pickle')

color = [(0.6509803921568628, 0.807843137254902, 0.8901960784313725),
         (0.12156862745098039, 0.47058823529411764, 0.7058823529411765),
         'k']



# #sería bueno ver cómo cambiar el font


# # plot de stffiness
# color = ['#BF399E', '#5899DA', '#E8743B']

plt.figure()
stiffnes['gaitLeg'] = stiffnes.leg+stiffnes.gait
k = stiffnes[~stiffnes.gaitLeg.isin(['trW']) & ~stiffnes.gaitLeg.isin(['ldW']) & ~stiffnes.gaitLeg.isin(['ldR'])]  
k['kn'] = k.k.values/1000
hue_order = ['trR', 'trS', 'ldS']

sns.pointplot(x='vel',y='kn',data=k,hue='gaitLeg',hue_order=hue_order,palette= sns.color_palette("Paired",4)[1:] ,ci='sd',errwidth=.7,capsize=.05)
g = sns.pointplot(x='vel',y='kn',data=k,hue='gaitLeg',hue_order=hue_order,palette= sns.color_palette("Paired",4)[1:] ,ci='sd',errwidth=.7,capsize=.05)

g.set_xlabel('Speed (km/h)')
g.set_ylabel('Stiffness (kN/m)')
# sns.set()
sns.set_style("ticks")
sns.set_context("paper",font_scale = 1.3)
sns.despine()
# g.text(5,40,'german pequera',size='medium',color='black')
leg_handles = g.get_legend_handles_labels()[0]
g.legend(leg_handles,['Running','Skipping (trailing)','Skipping (leading)'],frameon=False)
# plt.show()
plt.savefig('./figures/stiffness.pdf')
plt.savefig('./figures/stiffness.png')
plt.close()


# #plot de wavelets

muscles = data.muscle.unique()
leg = ['tr','ld']

colorstr = [
		  [(0.6509803921568628, 0.807843137254902, 0.8901960784313725), #walking color
 		  (0.6980392156862745, 0.8745098039215686, 0.5411764705882353)],   # trailing color
		 [(0.12156862745098039, 0.47058823529411764, 0.7058823529411765), # running color
		  (0.6509803921568628, 0.807843137254902, 0.8901960784313725),    # walking color
		  (0.6980392156862745, 0.8745098039215686, 0.5411764705882353)],  # trailing color
		 [(0.6980392156862745, 0.8745098039215686, 0.5411764705882353),   # trailing color
		  (0.12156862745098039, 0.47058823529411764, 0.7058823529411765)]  # running color
		 ]

colorsld = [
		  [(0.6509803921568628, 0.807843137254902, 0.8901960784313725), #walking color
 		  (0.2, 0.6274509803921569, 0.17254901960784313) ],   # trailing color
		 [(0.12156862745098039, 0.47058823529411764, 0.7058823529411765), # running color
		  (0.6509803921568628, 0.807843137254902, 0.8901960784313725),    # walking color
		  (0.2, 0.6274509803921569, 0.17254901960784313)],  # trailing color
		 [(0.2, 0.6274509803921569, 0.17254901960784313),   # trailing color
		  (0.12156862745098039, 0.47058823529411764, 0.7058823529411765)]  # running color
		 ]
 

row_order=["tr", "ld"]
speeds = np.round(np.array([6.5, 9])/3.6,2)

for c,v in enumerate(speeds):
	datavel = data[data.velmsec.isin([v])]
	g = sns.FacetGrid(datavel, row="leg", col="muscle",margin_titles=True, height=3,aspect=1,row_order=row_order)
	sns.set()
	sns.set_style("white")

	g = g.map(sns.barplot, '#_wavelet',"energy",'gait',ci='sd',errwidth=.7,capsize=.05, palette = colorstr[c],
		      hue_order=datavel.gait.unique(),order=datavel['#_wavelet'].unique(),edgecolor='k')
	
	g.add_legend()
	g.despine()
	statsV = gait_stats[gait_stats.vel.isin([str(v)])]
	# if v == 1.81:
	# 	gaitSignificative_stats = statsV[(statsV['p-corr']<.05)]
	# else:
	gaitSignificative_stats = statsV[(statsV['p-unc']<.05)]
	for muscle in gaitSignificative_stats.muscle.unique():
		figNumber = g.col_names.index(muscle)
		for row in gaitSignificative_stats[gaitSignificative_stats.muscle.isin([muscle])].iterrows():
			wave = row[1]['n_wavelet']
			ll = row[1]['leg']
			# if ('W' in row[1]['A']) or ('W' in row[1]['B']): 
			# 	if ll == 'tr':
			# 		g.axes.flatten()[figNumber].text(wave-1,.3,'*')
			# 	else:
			# 		g.axes.flatten()[figNumber+7].text(wave-1,.3,'*')
			# else:
			if ll == 'tr':
				g.axes.flatten()[figNumber].text(wave-1,.35,r'$\dag$')
			else:
				g.axes.flatten()[figNumber+7].text(wave-1,.35,r'$\dag$')
	plt.savefig('./figures/wavelets_'+str(v)+'.pdf')
	plt.savefig('./figures/wavelets_'+str(v)+'.png')
	plt.close()

colors = [(0.2, 0.6274509803921569, 0.17254901960784313),                 #trailing color
          (0.6980392156862745, 0.8745098039215686, 0.5411764705882353)    #leading color
                            
           ]
for c,v in enumerate(speeds):
	byside = data[data.gait.isin(['S']) & data.velmsec.isin([v])]
	sns.set()
	g = sns.FacetGrid(byside, col="muscle",col_wrap=4,margin_titles=True, height=3,aspect=1)
	sns.set_style("white")
	g = g.map(sns.barplot, '#_wavelet',"energy",'leg',ci='sd',errwidth=.7,capsize=.05,
			  hue_order = ['ld','tr'],order=byside['#_wavelet'].unique(),palette=colors,edgecolor='k')
	g.add_legend()
	g.despine()
	# leg_statsSign = 
	sideSkip_stats = leg_stats[(leg_stats.gait.isin(['S'])) & (leg_stats.vel.isin([v])) & (leg_stats.pval<.05)]
	for muscle in sideSkip_stats.muscle.unique():
		figNumber = g.col_names.index(muscle)
		for wave in sideSkip_stats[sideSkip_stats.muscle.isin([muscle])]['n_wavelet'].unique():
			g.axes.flatten()[figNumber].text(wave-1,.3,'*')
	plt.savefig('./figures/wavelets_'+str(v)+'_byside.pdf')
	plt.savefig('./figures/wavelets_'+str(v)+'_byside.png')
	plt.close()



for c,gait in enumerate(['Skipping','Running']):
	bygait = data[data.gait.isin([gait[0]])]
	sns.set()
	g = sns.FacetGrid(bygait, col="muscle",row='leg',margin_titles=True, height=3,aspect=1)
	sns.set_style("white")
	g = g.map(sns.barplot, '#_wavelet',"energy",'vel',ci='sd',errwidth=.7,capsize=.05,
			  hue_order=bygait.vel.sort_values().unique(),order=bygait['#_wavelet'].unique(),palette=['white','grey',[.8,.8,.8],'k'],edgecolor='k')
	g.add_legend()
	g.despine()
	statsG = vel_stats[vel_stats.gait.isin([gait[0]])]
	velSignificative_stats = statsG[(statsG['p-corr']<.05)]
	for muscle in velSignificative_stats.muscle.unique():
		figNumber = g.col_names.index(muscle)
		for row in velSignificative_stats[velSignificative_stats.muscle.isin([muscle])].iterrows():
			wave = row[1]['n_wavelet']
			ll = row[1]['leg']
			# if 'W' in row[1]['A'] or 'W' in row[1]['B']:
			if ll == 'tr':
				g.axes.flatten()[figNumber].text(wave-1,.3,'*')
			else:
				g.axes.flatten()[figNumber+7].text(wave-1,.3,'*')
	plt.suptitle(gait)
	plt.savefig('./figures/wavelets_'+str(gait[0])+'_bygait.pdf')
	plt.savefig('./figures/wavelets_'+str(gait[0])+'_bygait.png')
	plt.close()