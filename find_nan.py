import numpy as np
# import matplotlib.pyplot as plt
import pandas as pd
import glob
import os
# from progress.bar import FillingCirclesBar


path = '/home/german/ownCloud/work/phd/proyect_2016/data/colecta/npy/'
# folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/'
# # if not os.path.exists(folder):
# #         os.makedirs(folder)
# subjects = dict()
files = glob.glob(path + '*.npy')
n_files = len(files)
# bar = FillingCirclesBar('Processing', max=n_files)
dfs = []
ddffss = []
for file in files:
	filename = os.path.basename(file)[:-4]
	dic = np.load(file,encoding='latin1',allow_pickle=True).item()
	# key = list(dic.keys())[0]
	for i,ii in enumerate(dic['emg_r']):
		if ii.sum()[0]==0:
			df = pd.DataFrame()
			df['trial_id'] = [filename]
			df['#cicle'] = [i]
			dfs.append(df)

	for ciclor in dic['emg_r']:
		if 'Sensor 1' in list(ciclor.columns):
			ddff = pd.DataFrame()
			ddff['subject'] = [filename]
			# ddff['leg'] = 
			ddffss.append(ddff)
	for ciclol in dic['emg_l']:
		if 'Sensor 1' in list(ciclol.columns):
			ddff = pd.DataFrame()
			ddff['subject'] = [filename]
			# ddff['leg'] = 
			ddffss.append(ddff)

if len(dfs) != 0:
	DF = pd.concat(dfs,ignore_index=True)
	print(DF.pivot_table(index='trial_id',aggfunc=np.max))
if len(ddffss) != 0:
	DDFF = pd.concat(ddffss,ignore_index=True)
	print(DDFF['subject'])





