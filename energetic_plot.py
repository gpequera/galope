import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from pingouin import ttest
import pingouin as pg
from pingouin import welch_anova, kruskal
import scikit_posthocs as posthocs

energetic = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/energetic/energetic')
mech = energetic.reset_index().sort_values(by=['subject','gait','vel'])
Wint = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/energetic/wint')
mech['velmsec'] = np.round(mech.vel/3.6,2)
colors = ["#1B9E77",'#000000',"#D95F02"]

sns.set(style="white", font_scale=1)
err_kws = {'elinewidth':1,'capsize':3}
fig, (ax1,ax2,ax3) = plt.subplots(1,3,figsize=(15,5))

g1 = sns.lineplot(x='velmsec',y='Wext',hue='gait',ci= 'sd', data=mech, ax= ax1, palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
sns.despine()
g2 = sns.lineplot(x='vel',y='Wint',hue='gait', hue_order = ['R','S','W'],ci= 'sd', data=Wint, ax= ax2, palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
sns.despine()
g3 = sns.lineplot(x='velmsec',y='recovery',hue='gait',ci= 'sd', data=mech, ax= ax3 , palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
sns.despine()
ax1.set_xlabel('Speed (m.sec$^{-1}$)')
ax2.set_xlabel('Speed (m.sec$^{-1}$)')
ax3.set_xlabel('Speed (m.sec$^{-1}$)')
ax1.set_ylabel('Wext (J/Kg.m)')
ax2.set_ylabel('Wint (J/Kg.m)')
ax3.set_ylabel('Recovery (%)')

g1.legend_.remove()
g2.legend_.remove()
# g3.legend(title=None, loc='upper right')
g3.legend_.set_frame_on(False)
g3.legend_.set_title(None)

g1.text(1,1.8,'A',fontsize=20)
g1.text(1.39,1.8,'*',fontsize=12)
g1.text(1.78,1.8,'* $\S$ ',fontsize=12)
# g1.text(6.6,1.8,r'$\S$',fontsize=16)
g1.text(2.5,1.8,r'$\dag$',fontsize=12)

g2.text(1,.58,'B',fontsize=20)
g1.text(2.5,1.8,r'$\dag$',fontsize=16)
g2.text(1,.65,'B',fontsize=20)


g3.text(1,75,'C',fontsize=20)
g3.text(1.39,75,'*',fontsize=12)
g3.text(1.81,75,'* $\dag$  $\S$',fontsize=12)
# g2.text(,75,r'$\dag$, ',fontsize=16)
# g2.text(6.8,75,r'$\S$',fontsize=16)
g3.text(2.5,42,r'$\dag$',fontsize=12)

plt.tight_layout()

stats = pd.DataFrame()
for var in ['Wext','recovery']:
	for vel in [5, 9]:
		df = mech[mech.vel.isin([vel])]
		g = df.gait.unique()
		x = df[df.gait.isin([g[0]])][var].to_numpy()
		y = df[df.gait.isin([g[1]])][var].to_numpy()
		print(var,vel)
		print(ttest(x, y, tail='two-sided'))
	
	# aov = pg.anova()
	# print(aov)
df65 = mech[mech.vel.isin([6.5])]
for var in ['Wext']:#,'recovery']:
	# aov = welch_anova(dv=var, between='gait', data=df65)
	aov = kruskal(dv=var, between='gait', data=df65) #no variance homogenety
	# aov = pg.anova(dv=var, between='gait', data=df65)
	print('-----------------------------------------')
	print(var + ' 6.5')
	print(aov)
	r = df65[df65.gait.isin(['R'])][var]
	s = df65[df65.gait.isin(['S'])][var]
	w = df65[df65.gait.isin(['W'])][var]
	# print(posthocs.posthoc_ttest([r,s,w],p_adjust='bonferroni'))
	print(posthocs.posthoc_dunn(df65,val_col='Wext',group_col='gait',p_adjust='bonferroni'))
	print('-----------------------------------------')

statsWint = pd.DataFrame()
# for var in ['Wext','recovery']:
for vel in [1.39, 2.5]:
	df = Wint[Wint.vel.isin([vel])]
	g = df.gait.unique()
	x = df[df.gait.isin([g[0]])]['Wint'].to_numpy()
	y = df[df.gait.isin([g[1]])]['Wint'].to_numpy()
	print('Wint',vel)
	print(ttest(x, y, tail='two-sided'))
	print(pg.mwu(x, y, tail='two-sided'))

	
print('------------------------------------')
print('------------------------------------')
df65 = Wint[Wint.vel.isin([1.81])]
aov = welch_anova(dv='Wint', between='gait', data=df65)
# aov = kruskal(dv='Wint', between='gait', data=df65) #no variance homogenety
# aov = pg.anova(dv='Wint', between='gait', data=df65)
print('-----------------------------------------')
print( 'Wint 6.5')
print(aov)
r = df65[df65.gait.isin(['R'])]['Wint']
s = df65[df65.gait.isin(['S'])]['Wint']
w = df65[df65.gait.isin(['W'])]['Wint']
# print(posthocs.posthoc_ttest([r,s,w],p_adjust='bonferroni'))
print(posthocs.posthoc_dunn(df65,val_col='Wint',group_col='gait',p_adjust='bonferroni'))
print('-----------------------------------------')

# fig.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig6PeerJsyn.pdf')
# fig.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig6PeerJsyn.png')

fig.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/REfig7PeerJsyn.pdf')
fig.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/REfig7PeerJsyn.png')

plt.show()
