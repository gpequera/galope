import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt

cot = pd.read_pickle('../papersyn/CoT')
cot['velmsec'] = np.round(cot.vel/3.6,2)
energetic = pd.read_pickle('../papersyn/energetic')
mech = energetic.reset_index().sort_values(by=['subject','gait','vel'])
mech['velmsec'] = np.round(mech.vel/3.6,2)
W = pd.read_pickle('../papersyn/W')
eff = pd.DataFrame()
# cotmean = cot.groupby(['vel', 'gait']).mean().reset_index()
# Wmean = W.groupby(['vel', 'gait']).mean().reset_index()
# eff['eff'] = Wmean.wtotal/cotmean.COT
# eff['gait'] = cotmean.gait 
# eff['vel'] = cotmean.velmsec 

colors = ["#D95F02","#1B9E77",'#000000']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]

sns.set(style="white", font_scale=1)
err_kws = {'elinewidth':1,'capsize':3}
ylabel = ['COT (J/kg/m)','Metabolic power (W/kg)']
fig1,ax1 = plt.subplots(1,2,figsize=(6,2.5))
# plt.subplots_adjust(wspace=.4)
hue_order=['R','S']

for i,variable in enumerate(['COT','pot']):
	sns.stripplot(x='velmsec',y=variable,data=cot[cot.gait!='W'],hue='gait', hue_order=hue_order, ax=ax1[i],palette= "Set2", s=2.5)
	sns.pointplot(x="velmsec", y=variable, hue="gait", data=cot[cot.gait!='W'], hue_order=hue_order,markers='*',linestyles='--',palette="Set2",
		ax=ax1[i],jitter=True,dodge=True,ci='sd',errwidth=1,scale=0.6)
	ax1[i].set_ylabel(ylabel[i])
	ax1[i].set_xlabel('Speed (m.sec$^{-1}$)')
	ax1[i].legend_.remove()
handles, labels = ax1[1].get_legend_handles_labels()
l = plt.legend(handles[0:2], labels[0:2], bbox_to_anchor=(0.8, 0.3), loc=2, borderaxespad=0.,frameon=False)
sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.despine()
sns.set_style("ticks")
plt.savefig('./figures/cot.pdf')
plt.savefig('./figures/cot.png')
plt.show()
