import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


file = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wavelet_table'
data = pd.read_pickle(file)
stats = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/gait_stats')
muscles = data.muscle.unique()
colors = [['#5899DA', '#E8743B'],['#BF399E', '#5899DA', '#E8743B'],['#BF399E','#5899DA' ]]



for c,v in enumerate([5,6.5,9]): 
	fig, ax = plt.subplots(7,2)
	plt.suptitle(str(v) + ' km/h')
	for i,mm in enumerate(muscles):
		for ii,leg in enumerate(['tr','ld']):
			matrix = data[data.vel.isin([v]) & data.leg.isin([leg])].pivot_table(index=['muscle','#_wavelet'], columns='gait', values='energy')
			std = data[data.vel.isin([v]) & data.leg.isin([leg])].pivot_table(index=['muscle','#_wavelet'], columns='gait', values='energy',aggfunc=np.std).loc[mm]
			matrix.loc[mm].plot.bar(ax=ax[i,ii],legend=False,color=colors[c],ylim=[0,.5],yticks=[0,.5],yerr=std)
			tt = stats[stats.vel.isin([v]) & stats.muscle.isin([mm]) & stats.leg.isin([leg])]
			for index, row in tt.iterrows():
				ax[i,ii].text(row['#_wavelet']-1,.4,'*') 
			if ii == 0:
				ax[i,ii].text(-.25,.5,mm,transform=ax[i,ii].transAxes)
	ax[6,1].legend()
	ax[0,0].set_title('Trailing leg')
	ax[0,1].set_title('Leading leg')


plt.show()