import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


file = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wavelet_table'
data = pd.read_pickle(file)
stats = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/leg_stats')
s = stats[stats.gait.isin(['S'])]
muscles = data.muscle.unique()

# sns.set_context("paper")
# sns.set_context("poster")


matrix = data.pivot_table(index=['muscle','#_wavelet'], columns=['vel','gait','leg'], values='energy')
standard = data.pivot_table(index=['muscle','#_wavelet'], columns=['vel','gait','leg'], values='energy',aggfunc=np.std)
fig, ax = plt.subplots(7,3)
plt.suptitle('Skipping')
for c,v in enumerate([5,6.5,9]):
	x =  matrix[v]
	xstd = standard[v]
	for i,mm in enumerate(muscles):	
		xx = x['S'].loc[mm]
		std = xstd['S'].loc[mm]
		xx.plot.bar(ax=ax[i,c],color=['#000000','#FFFFFF'],legend=False, edgecolor = '#000000',yerr=std,ylim=[0,.5],yticks=[0,.5])
		if i==0:
			ax[i,c].set_title(str(v)+' km/h')
		if c==0:
			ax[i,c].text(-.4,.5,mm,transform=ax[i,c].transAxes)
		asterisco = s[s.vel.isin([v]) & s.muscle.isin([mm])]
		for index, row in asterisco.iterrows():
			ax[i,c].text(row['#_wavelet']-1,.4,'*',fontsize=12)
		ax[i,c].set_xlabel('')
		ax[i,c].set_xticks([])
		if i==6:
			ax[i,c].set_xlabel('Wavelet')
			ax[i,c].set_xticks(np.arange(1,12))
ax[6,2].legend()
sns.despine()

plt.show()






	# 		matrix = data[data.vel.isin([v]) & data.leg.isin([leg])].pivot_table(index=['muscle','#_wavelet'], columns='gait', values='energy')
			

	# 		std = data[data.vel.isin([v]) & data.leg.isin([leg])].pivot_table(index=['muscle','#_wavelet'], columns='gait', values='energy',aggfunc=np.std).loc[mm]
	# 		matrix.loc[mm].plot.bar(ax=ax[i,ii],legend=False,color=colors[c],ylim=[0,.5],yticks=[0,.5],yerr=std)
	# 		tt = stats[stats.vel.isin([v]) & stats.muscle.isin([mm]) & stats.leg.isin([leg])]
	# 		for index, row in tt.iterrows():
	# 			ax[i,ii].text(row['#_wavelet']-1,.4,'*') 
	# 		if ii == 0:
	# 			ax[i,ii].text(-.25,.5,mm,transform=ax[i,ii].transAxes)
	# ax[6,1].legend()
	# ax[0,0].set_title('Trailing leg')
	# ax[0,1].set_title('Leading leg')

