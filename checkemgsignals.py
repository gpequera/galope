import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
from scipy import signal

# path = '/home/german/Nextcloud/work/datosphd_npy/two_sides_emg/*.npy'
# files = glob.glob(path)




data = np.load('wavelet_res.npy',encoding='latin1',allow_pickle=True).item()
cond = ['W_030tr','W_040tr','W_050tr','W_065tr','R_065tr','R_090tr','R_110tr','S_050tr','S_065tr','S_090tr',
	'W_030ld','W_040ld','W_050ld','W_065ld','R_065ld','R_090ld','R_110ld','S_050ld','S_065ld','S_090ld']

for c in cond:
	subset = [s for s in list(data.keys()) if c in s]
	fig, ax = plt.subplots(7,20)
	fig.suptitle(c)
	for i,file in enumerate(subset):
		dic = data[file]
		dfs = []
		for ii,muscle in enumerate(list(dic.keys())):
			for iii in range(20):
				wavelist = dic[muscle]['wave']
				df = pd.DataFrame()
				df[muscle] = wavelist[iii].sum(axis=1).values			
				y = df[muscle].values
				ax[ii,iii].plot(signal.resample(y, 100),label=file[:2])
				ax[ii,iii].spines['right'].set_visible(False)
				ax[ii,iii].spines['top'].set_visible(False)
				ax[ii,iii].spines['left'].set_visible(False)
				ax[ii,iii].spines['bottom'].set_visible(False)
				ax[ii,iii].set_xticks([])
				ax[ii,iii].set_xticklabels([])
				ax[ii,iii].set_yticks([])
				ax[ii,iii].set_yticklabels([])
		dfs.append(df)
	emgcnc = pd.concat(wavelist,ignore_index=True)
	ax[6,19].legend()
	plt.show()
