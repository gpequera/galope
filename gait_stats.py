import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import researchpy as rp
import statsmodels.api as sm
from statsmodels.formula.api import ols
from statsmodels.stats.multicomp import MultiComparison
from scipy import stats
import pingouin as pg

# folder = './wavelet_results/'
file = './wavelet_table'
data = pd.read_pickle(file)
data['velmsec'] = np.round(data.vel/3.6,2)

normal_list = []
levene_list = []

muscles = data.muscle.unique()
n_wavelets = data['#_wavelet'].unique()
# vels = data.velmsec.unique()
legs = data.leg.unique()
for vel in [1.39, 1.81, 2.5]:
	for leg in legs:
		for muscle in muscles:
			for n_wavelet in n_wavelets:
				m = data[data.velmsec.isin([vel]) & data.leg.isin([leg]) & data.muscle.isin([muscle]) & data['#_wavelet'].isin([n_wavelet])]
				formula = 'energy ~ C(gait)'
				model = ols(formula, data=m).fit()
				norm = pg.normality(model.resid)
				norm['vel'] = [vel]
				norm['muscle'] = [muscle]
				norm['n_wavelet'] = n_wavelet
				norm['leg'] = [leg]
				levene = pg.homoscedasticity(m, dv='energy', group='gait')
				levene['vel'] = [vel]
				norm['levene'] = levene['equal_var'].item()
				normal_list.append(norm)
				levene_list.append(levene)
normal = pd.concat(normal_list,ignore_index=True)
# normal = normal[['var', 'vel','W', 'pval', 'normal', 'levene']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
levene_test = pd.concat(levene_list,ignore_index=True)
# levene_test = levene_test[['var', 'vel', 'W', 'pval', 'equal_var']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)

listaanovas = []
listakrukals = []
listapairwises = []

for index, row in normal.iterrows():
	vel = row['vel']
	muscle = row['muscle']
	n_wavelet = row['n_wavelet']
	leg = row['leg']
	subset= data[data.velmsec.isin([vel]) & data.muscle.isin([muscle]) & data['#_wavelet'].isin([n_wavelet]) & data.leg.isin([leg])]
	if len(subset.gait.unique())>2:
		if row['normal'] == True:
			if row['levene'] == False:
				anovaGait = pg.welch_anova(data=subset, dv='energy', between='gait').round(3)
			else:
				anovaGait = pg.anova(data=subset, dv='energy', between='gait', detailed=True).round(3).drop([1])
			anovaGait['muscle'] = [muscle]
			anovaGait['n_wavelet'] = [n_wavelet] 
			anovaGait['vel'] = [vel]
			anovaGait['leg'] = [leg]
			listaanovas.append(anovaGait)
			if anovaGait['p-unc'][0]<0.05:
						pairwiseGait = pg.pairwise_ttests(dv='energy', between='gait', data=subset, padjust='bonf',parametric=True).round(3)
						pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
						pairwiseGait['muscle'] = [muscle]  * pairwiseGait.shape[0]
						pairwiseGait['n_wavelet'] = [n_wavelet] * pairwiseGait.shape[0]
						pairwiseGait['leg'] = [leg] * pairwiseGait.shape[0]
						listapairwises.append(pairwiseGait)
		else:
			kruskalGait = pg.kruskal(data=subset, dv='energy', between='gait', detailed=True).round(3)
			kruskalGait['vel'] = [vel]
			kruskalGait['muscle'] = [muscle]
			kruskalGait['n_wavelet'] = [n_wavelet]
			kruskalGait['leg'] = [leg]
			listakrukals.append(kruskalGait)
			if kruskalGait['p-unc'][0]<0.05:
				pairwiseGait = pg.pairwise_ttests(dv='energy', between='gait', data=subset, padjust='bonf',parametric=False).round(3)
				pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
				pairwiseGait['muscle'] = [muscle] * pairwiseGait.shape[0]
				pairwiseGait['n_wavelet'] = [n_wavelet] * pairwiseGait.shape[0]
				pairwiseGait['leg'] = [leg] * pairwiseGait.shape[0]
				listapairwises.append(pairwiseGait)
	else:
		pairwiseGait = pg.pairwise_ttests(dv='energy', between='gait', data=subset, padjust='bonf',parametric=row['normal']).round(3)
		pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
		pairwiseGait['muscle'] = [muscle] * pairwiseGait.shape[0]
		pairwiseGait['n_wavelet'] = [n_wavelet] * pairwiseGait.shape[0]
		pairwiseGait['leg'] = [leg] * pairwiseGait.shape[0]
		listapairwises.append(pairwiseGait)				


anovas = pd.concat(listaanovas,ignore_index=True)
kruskals = pd.concat(listakrukals,ignore_index=True)
pairwises = pd.concat(listapairwises,ignore_index=True)


anovas.to_pickle('./stats/gait_anovastats.pickle')
kruskals.to_pickle('./stats/gait_kruskalstats.pickle')
pairwises.to_pickle('./stats/gait_stats.pickle')
