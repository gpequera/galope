import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os
from progress.bar import FillingCirclesBar


flag = 0

folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/'
folder1 = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/wave_per_trial/'
archivos = glob.glob(folder1 + '*.npy')
bar = FillingCirclesBar('Processing', max=len(archivos))
# data = {}
dfs = []
for file in archivos:
    name = os.path.basename(file)[8:-4]
    dic = np.load(file,allow_pickle=True,encoding='latin1').item()


    for mm in list(dic.keys()):
        df = pd.DataFrame()
        value = np.mean(dic[mm]['pj'],axis=0)
        mf_value = np.mean(dic[mm]['MF'])
        N = len(value)
        df['energy'] = value
        df['muscle'] = [mm]*N
        df['gait'] = [name[3]]*N
        df['leg'] = [name[-2:]]*N
        df['subject'] = [name[:2]]*N
        df['#_wavelet'] = np.arange(1,11)
        df['trial_id'] = name
        df["vel"] = np.float(name[5:8])/10*np.ones(N)
        dfs.append(df)
        bar.next()
masterDF = pd.concat(dfs,ignore_index=True)
masterDF.to_pickle(folder+'wavelet_table')
bar.finish()



if flag == 1:

    table = pd.pivot_table(masterDF,index=["#_wavelet","subject"],columns=["vel","muscle","gait","leg"],values=["energy"])
    table_means = pd.pivot_table(masterDF,index=["#_wavelet"],columns=["vel","muscle","gait","leg"],values=["energy"])
    table_std = pd.pivot_table(masterDF,index=["#_wavelet"],columns=["vel","muscle","gait","leg"],values=["energy"],aggfunc=np.std)


    colors = [['W','#eba823'],['S','#e2527b'],['R','#4f529e']]  
    hatchs = [['tr','///'],['ld',None]]
    for v in list(table.columns.levels[1]):
        f, ax = plt.subplots(4,2,sharex=True, figsize=(18,8))
        f.subplots_adjust(hspace=0)
        plt.suptitle('Speed ' + str(v) + ' km/h')
        ax = ax.ravel()

        for i,m in zip(range(6),list(table.columns.levels[2])):
            color = []
            hatch = []
            toplot = table_means["energy"][v][m]
            n = toplot.shape[0]
            g = list(toplot.columns.get_level_values(0))
            l = list(toplot.columns.get_level_values(1))
            for gg,ll in zip(g,l):
                color.append([s for s in colors if gg in s][0][1])
                hatch.extend([[h for h in hatchs if ll in h][0][1]]*n)
            yerr = table_std["energy"][v][m]
            p = toplot.plot.bar(ax=ax[i],yerr=yerr,color=color, edgecolor='k',legend=False, error_kw=dict(ecolor='black',elinewidth=0.5, lolims=True))
            for ch in p.get_lines():
                ch.set_marker('_')
                ch.set_markersize(2) # to change its size
            for nn,ll in enumerate(p.patches):
                ll.set_hatch(hatch[nn])

            ax[i].text(0.5, 0.9, m, verticalalignment='center', horizontalalignment='center',transform=ax[i].transAxes,fontsize=13)
        plt.legend()
        # plt.show()
        plt.savefig(folder+'pj_v_'+ str(v) + '.pdf', dpi=300)