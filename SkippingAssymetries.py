#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import seaborn as sns
import numpy  as np
import matplotlib.pyplot as plt
import glob
import os


# In[92]:

spt = pd.read_pickle('/home/german/Documents/repo/papersyn/spatiotemporal')
duty = spt[spt.gait=='S'].reset_index(drop=True).sort_values(by=['subject','vel']).reset_index(drop=True)
folder = '/home/german/Nextcloud/work/datosphd_npy/'
DFS = []
Tstep1 = []
Tstep2 = []
DeltaStep = []
Lstep = []
Lflight = []
DeltaLength = []
skippings = glob.glob(folder+'*_S_*.npy')
runnings = glob.glob(folder+'*_R_*.npy')
walkings = glob.glob(folder+'*_W_*.npy')
files = runnings+skippings
for file in files:
    if 'CC' not in file:
        file_id = os.path.basename(file)[:-4]
        data = np.load(file,encoding='latin1',allow_pickle=True).item()
        info = data.get('info')
        m = info['mass (kg)'].values.item()
        fs = info['fs_kin (Hz)'].values.item()
        # fs = info['fs_kin'].values.item()
        trailing = info['trailing_leg'].values.item()
        subject = info['file_name'].values.item()[:2]
        vel = info['vel (km/h)'].values.item()
        gait = info['gait'].values.item().upper()
        dt = 1/fs
        if trailing == 'l':
            side = 'kinematic_l'
            col = ['LANy','RANy','LHEEz','RHEEz']
        else:
            side = 'kinematic_r'
            col = ['RANy','LANy','RHEEz','LHEEz']
        for stride in data[side][15:35]:
            markers = stride[col].reset_index(drop=True)
            markers.columns = ['trANy', 'ldANy', 'trHEEz', 'ldHEEz']
            HeelStrike_ld = markers['ldHEEz'].argmin()
            t1 = HeelStrike_ld*dt
            t2 = (markers.index[-1]-HeelStrike_ld)*dt
            Tstep1.append(t1)
            Tstep2.append(t2)
            deltat = (t2-t1)/(.5*(t1+t2))
            DeltaStep.append(deltat)
            l1 = markers['ldANy'].iloc[HeelStrike_ld]-markers['trANy'].iloc[HeelStrike_ld]
            if gait == 'R':
                l2 = markers['trANy'].iloc[-1]-markers['ldANy'].iloc[-1]
            else:
                l2 = markers['ldANy'].iloc[-1]-markers['trANy'].iloc[-1]
            deltal = (l1-l2)/(.5*(l1+l2))
            Lstep.append(l1)
            Lflight.append(l2)
            DeltaLength.append(deltal)          
        Tsteptr = np.mean(Tstep1)
        Tstepld = np.mean(Tstep2)
        assymt = np.mean(DeltaStep)
        Ltr = np.mean(Lstep)
        Lld = np.mean(Lflight)
        assyml = np.mean(DeltaLength)
        # if assyml<.60:
        #     print(info['file_name'])
        #     print(trailing)
        df = pd.DataFrame()
        df['subject'] = [subject]
        df['gait'] = [gait]
        df['vel'] = [vel]
        df['Tsteptr'] = [Tsteptr]
        df['Tstepld'] = [Tstepld]
        df['assymt'] = [assymt]
        df['Lsteptr'] = [Ltr]
        df['Lstepld'] = [Lld]
        df['assyml'] = [assyml]
        DFS.append(df)
assymetries = pd.concat(DFS,ignore_index=True).sort_values(by=['subject','vel']).reset_index(drop=True)
m = assymetries.set_index(['subject','vel','gait']).join(duty.set_index(['subject','vel','gait'])).reset_index() 
m.to_pickle('./assymetries.pickle')
# sns.lmplot(x='assyml',y='assymt',hue='vel',data=assymetries,palette='bright')



