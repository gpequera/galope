import numpy as np
import os
import glob
from progress.bar import FillingCirclesBar
import pickle

folder = './two_sides_emg/'
if not os.path.exists(folder):
    os.makedirs(folder)

files = './fulldata.npy'

valueslist = []

d = np.load(files,encoding='latin1',allow_pickle=True).item()
conds = list(d.keys())
n_files = len(conds)
new_conds = []
for file in conds:
	data = d[file]
	if 'emg_l' in list(data.keys()):
		new_conds.append(file)
		for ciclor in data['emg_r']:
			if 'Sensor 1' in list(ciclor.columns):
				ciclor.columns = ['GasMedR', 'SOLR', 'RFR', 'VasMedR', 'TIBR', 'BFR', 'GluMedioR', 'GasMedL', 'SOLL', 'RFL', 'VasMedL', 'TIBL', 'BFL', 'GluMedL']
		for ciclol in data['emg_l']:
			if 'Sensor 1' in list(ciclol.columns):
				ciclol.columns = ['GasMedR', 'SOLR', 'RFR', 'VasMedR', 'TIBR', 'BFR', 'GluMedioR', 'GasMedL', 'SOLL', 'RFL', 'VasMedL', 'TIBL', 'BFL', 'GluMedL']


bar = FillingCirclesBar('Processing', max=n_files)
for file in new_conds:	
	# if 'emg_l' in list(data.keys()):
		bar.next()
		data = d[file]
		ff = os.path.basename(file)

		musclesL = [s for s in list(data.get('emg_l')[0].keys()) if 'L' in s[-1]]
		musclesR = [s for s in list(data.get('emg_l')[0].keys()) if 'R' in s[-1]]
		markersR = [s for s in list(data.get('kinematic_l')[0].keys()) if 'R' in s[0]]
		markersL = [s for s in list(data.get('kinematic_l')[0].keys()) if 'L' in s[0]]

		if data.get('info')['trailing_leg'][0] == 'l':
			emgld = [i[musclesR] for i in data.get('emg_r')]
			emgtr = [i[musclesL] for i in data.get('emg_l')]
			markersld = [i[markersR] for i in data.get('kinematic_r')]
			markerstr = [i[markersL] for i in data.get('kinematic_l')]
		else:
			emgld = [i[musclesL] for i in data.get('emg_l')]
			emgtr = [i[musclesR] for i in data.get('emg_r')]
			markersld = [i[markersL] for i in data.get('kinematic_l')]
			markerstr = [i[markersR] for i in data.get('kinematic_r')]

		muscle_names = list(data.get('emg_r')[0][musclesL].columns)
		marker_names = list(data.get('kinematic_r')[0][markersL].columns)
		muscle = []
		marker = []
		# if len(markerstr)!=len(markersld):
		# 	print('markers_ '+ file + '-->'+str(len(markersld)))
		# if len(emgtr)!=len(emgld):
		# 	print('emg_ '+ file + '-->'+str(len(emgld)))



		for ii in muscle_names:
			muscle.append(ii[:-1])
		for ii in marker_names:
			marker.append(ii[1:])
		for j,k in zip(emgld,emgtr): 
			j.columns = muscle
			k.columns = muscle
		for j,k in zip(markersld,markerstr): 	
			j.columns = marker
			k.columns = marker
			
				
		name = dict({'emg_ld':emgld, 'emg_tr':emgtr, 'marker_ld':markersld, 'marker_tr':markerstr, 'info': data['info']})
		valueslist.append(name)
		
bar.finish()
# 	ld = dict({'ld':emgld})
# 	tr = dict({'tr':emgtr})
dic = dict(zip(new_conds,valueslist))
pickle.dump(dic, open("./dataBYleg.pickle", "wb"))
# np.save('./dataBYleg.npy',dic)
# 	np.save(folder+ff+'tr.npy',tr)
# 	bar.next()

