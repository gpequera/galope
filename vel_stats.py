import pandas as pd
import numpy as np
import pingouin as pg
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.formula.api import ols
from statsmodels.stats.multicomp import MultiComparison


# folder = '/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/'
file = './wavelet_table'
data = pd.read_pickle(file)
data['velmsec'] = np.round(data.vel/3.6,2)
lista = []
dfs = []
dafs = []

gaits = data.gait.unique() 
muscles = data.muscle.unique()
n_wavelets = data['#_wavelet'].unique()
legs = data.leg.unique()

normal_list = []
levene_list = []


for gait in gaits:
	for leg in legs:
		for muscle in muscles:
			for n_wavelet in n_wavelets:
				m = data[data.gait.isin([gait]) & data.leg.isin([leg]) & data.muscle.isin([muscle]) & data['#_wavelet'].isin([n_wavelet])]
				formula = 'energy ~ C(vel)'
				model = ols(formula, data=m).fit()
				norm = pg.normality(model.resid)
				norm['gait'] = [gait]
				norm['muscle'] = [muscle]
				norm['n_wavelet'] = [n_wavelet]
				norm['leg'] = [leg]
				levene = pg.homoscedasticity(m, dv='energy', group='vel')
				levene['gait'] = [gait]
				norm['levene'] = levene['equal_var'].item()
				normal_list.append(norm)
				levene_list.append(levene)
normal = pd.concat(normal_list,ignore_index=True)
levene_test = pd.concat(levene_list,ignore_index=True)



listaanovas = []
listakrukals = []
listapairwises = []

for index, row in normal.iterrows():
	gait = row['gait']
	muscle = row['muscle']
	n_wavelet = row['n_wavelet']
	leg = row['leg']
	subset= data[data.gait.isin([gait]) & data.muscle.isin([muscle]) & data['#_wavelet'].isin([n_wavelet]) & data.leg.isin([leg])]
	# if len(subset.vel.unique())>2:
	if row['normal'] == True:
		if row['levene'] == False:
			anovaVel = pg.welch_anova(data=subset, dv='energy', between='vel').round(3)
		else:
			anovaVel = pg.anova(data=subset, dv='energy', between='vel', detailed=True).round(3).drop([1])
		anovaVel['muscle'] = [muscle]
		anovaVel['n_wavelet'] = [n_wavelet] 
		anovaVel['gait'] = [gait]
		anovaVel['leg'] = [leg]
		listaanovas.append(anovaVel)
		if anovaVel['p-unc'][0]<0.05:
					pairwiseVel = pg.pairwise_ttests(dv='energy', between='vel', data=subset, padjust='bonf',parametric=True).round(3)
					pairwiseVel['gait'] = [gait] * pairwiseVel.shape[0]
					pairwiseVel['muscle'] = [muscle]  * pairwiseVel.shape[0]
					pairwiseVel['n_wavelet'] = [n_wavelet] * pairwiseVel.shape[0]
					pairwiseVel['leg'] = [leg] * pairwiseVel.shape[0]
					listapairwises.append(pairwiseVel)
	else:
		kruskalVel = pg.kruskal(data=subset, dv='energy', between='vel', detailed=True).round(3)
		kruskalVel['gait'] = [gait]
		kruskalVel['muscle'] = [muscle]
		kruskalVel['n_wavelet'] = [n_wavelet]
		kruskalVel['leg'] = [leg]
		listakrukals.append(kruskalVel)
		if kruskalVel['p-unc'][0]<0.05:
			pairwiseVel = pg.pairwise_ttests(dv='energy', between='vel', data=subset, padjust='bonf',parametric=False).round(3)
			pairwiseVel['gait'] = [gait] * pairwiseVel.shape[0]
			pairwiseVel['muscle'] = [muscle] * pairwiseVel.shape[0]
			pairwiseVel['n_wavelet'] = [n_wavelet] * pairwiseVel.shape[0]
			pairwiseVel['leg'] = [leg] * pairwiseVel.shape[0]
			listapairwises.append(pairwiseVel)

anovas = pd.concat(listaanovas,ignore_index=True)
anovas.to_pickle('./stats/vel_anovastats.pickle')
kruskals = pd.concat(listakrukals,ignore_index=True)
kruskals.to_pickle('./stats/vel_kruskalstats.pickle')
pairwises = pd.concat(listapairwises,ignore_index=True)
pairwises.to_pickle('./stats/vel_stats.pickle')