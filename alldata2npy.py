import numpy as np
import os
import glob
from progress.bar import FillingCirclesBar

folder = '/home/german/Nextcloud/work/phd_data/datosphd_npy/two_sides_emg/'

files = glob.glob(folder+'*.npy')
n_files = len(files)
bar = FillingCirclesBar('Processing', max=n_files)
conds = []
lista = []
for file in files:
    data = np.load(file,encoding='latin1',allow_pickle=True).item()
    file_name = os.path.basename(file)[:-4]
    lista.append(data)
    conds.append(file_name)
    bar.next()

bar.finish()
dic = dict(zip(conds,lista))

np.save('emg_two_sides_data.npy',dic)