#COT stats
import pandas as pd
import numpy as np
import pingouin as pg
import matplotlib.pyplot as plt 


cot = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/k5_data/CoT')
# cot = cot[~cot.gait.isin(['W'])]  
# cot['gaitLeg'] = cot.gait + cot.leg
# cot = cot[~cot.gaitLeg.isin(['Rld'])]

# lista = []

# for v in [5, 6.5, 9]:
# 	stats = cot[cot.vel.isin([v])]
# 	aov = pg.anova(data=stats, dv='k', between='gaitLeg')
# 	if aov['p-unc'][0]<.05:
# 		posthocs = pg.pairwise_ttests(dv='k', between='gaitLeg',padjust='bonf', data=stats)
# 		for index, row in posthocs.iterrows():
# 			if row['p-unc'] < .05:
# 				df = pd.DataFrame()
# 				df['vel'] = [v]
# 				df['A'] = row['A']
# 				df['B'] = row['B']
# 				df['p-unc'] = row['p-unc']
# 				lista.append(df)

# cotstats = pd.concat(lista,ignore_index=True)
# cotstats.to_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/wavelet_results/cot_stats')