import pandas as pd
import pingouin as pg
import statsmodels.api as sm
from statsmodels.formula.api import ols
import scipy.stats as stats
import numpy as np

file = './wavelet_table.pickle'
data = pd.read_pickle(file)
data['velmsec'] = np.round(data.vel/3.6,2)
wavelets = data.pivot_table(index=['muscle','gait','leg','vel','subject'],values='intensity',columns='n_wavelet') 

muscles = list(data['muscle'].unique())
gaits = list(data['gait'].unique())
n_wavelets = list(data['n_wavelet'].unique())
# speeds = [1.39, 1.81, 2.5]

normal_list = []
levene_list = []

dfs = []

for gait in gaits:
	mgait = data[data.gait.isin([gait])]
	for muscle in muscles:
		for vel in mgait.velmsec.unique():
			for n_wavelet in n_wavelets:
				m = mgait[mgait.muscle.isin([muscle]) & mgait.gait.isin([gait]) & mgait.velmsec.isin([vel]) & mgait['n_wavelet'].isin([n_wavelet])] 
				formula = 'intensity ~ C(leg)'
				model = ols(formula, data=m).fit()
				norm = pg.normality(model.resid)
				norm['vel'] = [vel]
				norm['muscle'] = [muscle]
				norm['n_wavelet'] = n_wavelet
				norm['gait'] = [gait]
				levene = pg.homoscedasticity(m, dv='intensity', group='leg')
				levene['vel'] = [vel]
				levene['gait'] = [gait]
				norm['levene'] = levene['equal_var'].item()
				normal_list.append(norm)
				levene_list.append(levene)
				a = m[m.leg=='ld'].intensity.values
				b = m[m.leg=='tr'].intensity.values
				df = pd.DataFrame()
				if norm.normal[0]:
					df['pval'] = pg.ttest(a, b, paired=False, tail='two-sided').round(3)['p-val']
				else:
					df['pval'] = pg.mwu(a, b, tail='two-sided')['p-val']
				df['gait'] = [gait]
				df['vel'] = [vel]
				df['muscle'] = [muscle]
				df['n_wavelet'] = [n_wavelet]
				dfs.append(df)
normal = pd.concat(normal_list,ignore_index=True)
levene_test = pd.concat(levene_list,ignore_index=True)
stats = pd.concat(dfs,ignore_index=True)



stats.to_pickle('./stats/leg_stats.pickle')

